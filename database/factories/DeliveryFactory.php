<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Models\Delivery::class, function (Faker $faker) {
    return [
        'order_id' => $faker->randomNumber(8),
        'email' => $faker->unique()->safeEmail,
        'postal_code' => $faker->postcode,
        'delivery_date' => $faker->dateTimeBetween(new DateTime('now'), new DateTime('+2 months')),
        'is_delivery_notified' => $faker->boolean(65),
        'is_canceled' => $faker->boolean(20),
        'is_cancellation_notified' => $faker->boolean(2),
        'canceled_at' => $faker->dateTimeBetween(new DateTime('now'), new DateTime('+2 months')),
    ];
});
