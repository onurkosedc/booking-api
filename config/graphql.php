<?php

declare(strict_types=1);

use example\Mutation\ExampleMutation;
use example\Query\ExampleQuery;
use example\Type\ExampleRelationType;
use example\Type\ExampleType;
use Laravel\Sanctum\Http\Middleware\EnsureFrontendRequestsAreStateful;

return [

    // The prefix for routes
    'prefix' => 'graphql',

    // The routes to make GraphQL request. Either a string that will apply
    // to both query and mutation or an array containing the key 'query' and/or
    // 'mutation' with the according Route
    //
    // Example:
    //
    // Same route for both query and mutation
    //
    // 'routes' => 'path/to/query/{graphql_schema?}',
    //
    // or define each route
    //
    // 'routes' => [
    //     'query' => 'query/{graphql_schema?}',
    //     'mutation' => 'mutation/{graphql_schema?}',
    // ]
    //
    'routes' => '{graphql_schema?}',

    // The controller to use in GraphQL request. Either a string that will apply
    // to both query and mutation or an array containing the key 'query' and/or
    // 'mutation' with the according Controller and method
    //
    // Example:
    //
    // 'controllers' => [
    //     'query' => '\Rebing\GraphQL\GraphQLController@query',
    //     'mutation' => '\Rebing\GraphQL\GraphQLController@mutation'
    // ]
    //
    'controllers' => \Rebing\GraphQL\GraphQLController::class.'@query',

    // Any middleware for the graphql route group
    'middleware' => [
    ],

    // Additional route group attributes
    //
    // Example:
    //
    // 'route_group_attributes' => ['guard' => 'api']
    //
    'route_group_attributes' => [],

    // The name of the default schema used when no argument is provided
    // to GraphQL::schema() or when the route is used without the graphql_schema
    // parameter.
    'default_schema' => 'default',

    // The schemas for query and/or mutation. It expects an array of schemas to provide
    // both the 'query' fields and the 'mutation' fields.
    //
    // You can also provide a middleware that will only apply to the given schema
    //
    // Example:
    //
    //  'schema' => 'default',
    //
    //  'schemas' => [
    //      'default' => [
    //          'query' => [
    //              'users' => 'App\GraphQL\Query\UsersQuery'
    //          ],
    //          'mutation' => [
    //
    //          ]
    //      ],
    //      'user' => [
    //          'query' => [
    //              'profile' => 'App\GraphQL\Query\ProfileQuery'
    //          ],
    //          'mutation' => [
    //
    //          ],
    //          'middleware' => ['auth'],
    //      ],
    //      'user/me' => [
    //          'query' => [
    //              'profile' => 'App\GraphQL\Query\MyProfileQuery'
    //          ],
    //          'mutation' => [
    //
    //          ],
    //          'middleware' => ['auth'],
    //      ],
    //  ]
    //
    'schemas' => [
        'default' => [
            'query' => [],
            'mutation' => [],
            'middleware' => [],
            'method' => ['get', 'post'],
        ],

        'units' => [
            'query' => [
                'allUnits' => App\GraphQL\Queries\Unit\All::class,
            ],
            'mutation' => [],
            'middleware' => ['throttle']
        ],

        '_units' => [
            'query' => [],
            'mutation' => [
                'storeUnit' => App\GraphQL\Mutations\Unit\Store::class,
                'updateUnit' => App\GraphQL\Mutations\Unit\Update::class,
                'deleteUnit' => App\GraphQL\Mutations\Unit\Delete::class,
            ],
            'middleware' => ['throttle', 'auth:sanctum']
        ],

        'business_hours' => [
            'query' => [
                'allBusinessHours' => App\GraphQL\Queries\BusinessHour\All::class,
            ],
            'mutation' => [],
            'middleware' => ['throttle']
        ],

        '_business_hours' => [
            'query' => [],
            'mutation' => [
                'populateBusinessHours' => App\GraphQL\Mutations\BusinessHour\Populate::class,
                'deleteBusinessHour' => App\GraphQL\Mutations\BusinessHour\Delete::class,
                'updateBusinessHours' => App\GraphQL\Mutations\BusinessHour\Update::class,
            ],
            'middleware' => ['throttle', 'auth:sanctum']
        ],

        '_fast_track' => [
            'query' => [
                'allFastTrack' => App\GraphQL\Queries\FastTrack\All::class,
            ],
            'mutation' => [
                'storeFastTrack' => App\GraphQL\Mutations\FastTrack\Store::class,
                'deleteFastTrack' => App\GraphQL\Mutations\FastTrack\Delete::class,
            ],
            'middleware' => ['throttle', 'auth:sanctum']
        ],

        'settings' => [
            'query' => [
                'allSettings' => App\GraphQL\Queries\Setting\All::class,
            ],
            'mutation' => [],
            'middleware' => ['throttle']
        ],

        '_settings' => [
            'query' => [],
            'mutation' => [
                'storeSettings' => App\GraphQL\Mutations\Setting\Store::class,
                'uploadFile' => App\GraphQL\Mutations\Setting\UploadFile::class,
            ],
            'middleware' => ['throttle', 'auth:sanctum']
        ],

        'unit_schedule' => [
            'query' => [
                'allSchedule' => App\GraphQL\Queries\Unit\Schedule\All::class,
            ],
            'mutation' => [],
            'middleware' => ['throttle']
        ],

        '_unit_schedule' => [
            'query' => [],
            'mutation' => [
                'storeSchedule' => App\GraphQL\Mutations\Unit\Schedule\Store::class,
                'deleteSchedule' => App\GraphQL\Mutations\Unit\Schedule\Delete::class,
            ],
            'middleware' => ['throttle', 'auth:sanctum']
        ],

        'orders' => [
            'query' => [
                'checkOrder' => App\GraphQL\Queries\Order\Check::class,
                'getOrder' => App\GraphQL\Queries\Order\Get::class,
                'allOrderLines' => App\GraphQL\Queries\Order\Line\All::class
            ],
            'mutation' => [],
            'middleware' => ['throttle']
        ],

        '_orders' => [
            'query' => [
                'checkOrder' => App\GraphQL\Queries\_Order\Check::class,
            ],
            'mutation' => [],
            'middleware' => ['throttle']
        ],

        '_invitations' => [
            'query' => [],
            'mutation' => [
                'sendInvitation' => App\GraphQL\Mutations\Invitation\Send::class,
            ],
            'middleware' => ['throttle']
        ],

        'deliveries' => [
            'query' => [
                'dailyDeliveries' => App\GraphQL\Queries\Delivery\Daily::class,
            ],
            'mutation' => [
                'storeDelivery' => App\GraphQL\Mutations\Delivery\Store::class,
                'updateDeliveryOrderLines' => App\GraphQL\Mutations\Delivery\UpdateOrderLine::class,
                'updateDeliveryByVendor' => App\GraphQL\Mutations\Delivery\UpdateVendor::class,
            ],
            'middleware' => ['throttle']
        ],

        '_deliveries' => [
            'query' => [
                'getDelivery' => App\GraphQL\Queries\Delivery\Get::class,
                'paginateDeliveries' => App\GraphQL\Queries\Delivery\Paginate::class,
                'groupedDeliveries' => App\GraphQL\Queries\Delivery\Grouped::class,
            ],
            'mutation' => [
                'updateDelivery' => App\GraphQL\Mutations\Delivery\Update::class,
            ],
            'middleware' => ['throttle', 'auth:sanctum']
        ],
    ],

    // The types available in the application. You can then access it from the
    // facade like this: GraphQL::type('user')
    //
    // Example:
    //
    // 'types' => [
    //     'user' => 'App\GraphQL\Type\UserType'
    // ]
    //
    'types' => [
        // \Rebing\GraphQL\Support\UploadType::class,

        'unit' => App\GraphQL\Types\Unit\Unit::class,
            'unit_input' => App\GraphQL\Types\Unit\Input\Unit::class,

            'unit_schedule' => App\GraphQL\Types\Unit\Schedule\Schedule::class,
                'unit_schedule_input' => App\GraphQL\Types\Unit\Schedule\Input\Schedule::class,

        'fast_track' => App\GraphQL\Types\FastTrack\FastTrack::class,

        'business_hour' => App\GraphQL\Types\BusinessHour\BusinessHour::class,
            'business_hour_input' => App\GraphQL\Types\BusinessHour\Input\BusinessHour::class,


        'delivery' => App\GraphQL\Types\Delivery\Delivery::class,
            'delivery_input' => App\GraphQL\Types\Delivery\Input\Delivery::class,

            'delivery_group' => App\GraphQL\Types\Delivery\Group::class,

            'delivery_unit' => App\GraphQL\Types\Delivery\Unit::class,
            'delivery_order_line' => App\GraphQL\Types\Delivery\OrderLine::class,


        'temp_order' => \App\GraphQL\Types\Order\TempOrder::class,

        'order' => \App\GraphQL\Types\Order\Order::class,
            'order_line' => \App\GraphQL\Types\Order\Line\Line::class,
                'order_line_input' => \App\GraphQL\Types\Order\Line\Input\Line::class,


        'user' => \App\GraphQL\Types\User\User::class,
            'user_input' => \App\GraphQL\Types\User\Input\User::class,

            'user_setting_group' => \App\GraphQL\Types\User\Setting\Group::class,
            'user_setting_first_level' => \App\GraphQL\Types\User\Setting\FirstLevel::class,
            'user_setting_second_level' => \App\GraphQL\Types\User\Setting\SecondLevel::class,
            'user_setting_third_level' => \App\GraphQL\Types\User\Setting\ThirdLevel::class,
                'user_setting_input' => \App\GraphQL\Types\User\Input\Setting::class,

        'setting' => App\GraphQL\Types\Setting\Setting::class,
            'file' => App\GraphQL\Types\Setting\File::class,
            'setting_input' => App\GraphQL\Types\Setting\Input\Setting::class,


        'generic_CRUD' => App\GraphQL\Types\Generic\CRUD::class,
        'generic_message' => App\GraphQL\Types\Generic\Message::class,
            'generic_filter_input' => App\GraphQL\Types\Generic\Input\Filter::class,
    ],

    // The types will be loaded on demand. Default is to load all types on each request
    // Can increase performance on schemes with many types
    // Presupposes the config type key to match the type class name property
    'lazyload_types' => false,

    // This callable will be passed the Error object for each errors GraphQL catch.
    // The method should return an array representing the error.
    // Typically:
    // [
    //     'message' => '',
    //     'locations' => []
    // ]
    'error_formatter' => ['\Rebing\GraphQL\GraphQL', 'formatError'],

    /*
     * Custom Error Handling
     *
     * Expected handler signature is: function (array $errors, callable $formatter): array
     *
     * The default handler will pass exceptions to laravel Error Handling mechanism
     */
    'errors_handler' => ['\Rebing\GraphQL\GraphQL', 'handleErrors'],

    // You can set the key, which will be used to retrieve the dynamic variables
    'params_key' => 'variables',

    /*
     * Options to limit the query complexity and depth. See the doc
     * @ https://webonyx.github.io/graphql-php/security
     * for details. Disabled by default.
     */
    'security' => [
        'query_max_complexity' => null,
        'query_max_depth' => null,
        'disable_introspection' => false,
    ],

    /*
     * You can define your own pagination type.
     * Reference \Rebing\GraphQL\Support\PaginationType::class
     */
    'pagination_type' => \Rebing\GraphQL\Support\PaginationType::class,

    /*
     * Config for GraphiQL (see (https://github.com/graphql/graphiql).
     */
    'graphiql' => [
        'prefix' => '/graphiql',
        'controller' => \Rebing\GraphQL\GraphQLController::class.'@graphiql',
        'middleware' => [],
        'view' => 'graphql::graphiql',
        'display' => env('ENABLE_GRAPHIQL', true),
    ],

    /*
     * Overrides the default field resolver
     * See http://webonyx.github.io/graphql-php/data-fetching/#default-field-resolver
     *
     * Example:
     *
     * ```php
     * 'defaultFieldResolver' => function ($root, $args, $context, $info) {
     * },
     * ```
     * or
     * ```php
     * 'defaultFieldResolver' => [SomeKlass::class, 'someMethod'],
     * ```
     */
    'defaultFieldResolver' => null,

    /*
     * Any headers that will be added to the response returned by the default controller
     */
    'headers' => [],

    /*
     * Any JSON encoding options when returning a response from the default controller
     * See http://php.net/manual/function.json-encode.php for the full list of options
     */
    'json_encoding_options' => 0,
];
