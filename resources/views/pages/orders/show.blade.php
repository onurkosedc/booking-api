@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                <div class="card">
                    <div class="card-header">
                        #{{$orderModel->order_number}} &mdash; Order Lines
                        <div class="float-right">
                        </div>
                    </div>

                    <table class="table table-hover" width="100%">
                        <thead>
                        <tr>
                            <th scope="col">&nbsp;</th>
                            <th scope="col">Code</th>
                            <th scope="col">Description</th>
                            <th scope="col">Qty/Unit</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach ($orderListing as $item)
                            <tr>
                                <td>
                                    <div class="form-check">
                                        <input class="form-check-input position-static" type="checkbox" id="blankCheckbox" value="option1" aria-label="...">
                                    </div>
                                </td>
                                <td>{{$item['Vendor_Item_No']}}</td>
                                <td>{{$item['Description']}}</td>
                                <td>{{$item['Quantity']}} {{$item['Unit_of_Measure']}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <div class="card-footer align-items-center">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
