@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                <div class="card">
                    <div class="card-header">
                        Units
                        <div class="float-right">
                            <b-btn size="sm" @click="">Add new</b-btn>
                        </div>
                    </div>

                    <table class="table table-hover" width="100%">
                        <thead>
                        <tr>
                            <th scope="col" width="10%">ID</th>
                            <th scope="col">Code</th>
                            <th scope="col">Name</th>
                            <th scope="col" width="10%">&nbsp;</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach ($units as $unit)
                            <tr>
                                <td>{{$unit->id}}</td>
                                <td>{{$unit->code}}</td>
                                <td>{{$unit->email}}</td>
                                <td class="text-right">
                                    <delivery-listing-actions :delivery-id="{{$unit->id}}"></delivery-listing-actions>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <div class="card-footer align-items-center">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
