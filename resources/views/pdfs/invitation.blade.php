<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Purchase Order {{data_get($order, 'No')}}</title>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <style>
        @page {
            margin: 75px 0 0;
            padding-bottom: 10px;
            background-color: #ffffff;
            font-family: Arial, Helvetica, sans-serif;
        }

        .header {
            left: 0;
            right: 0;
            top: -75px;
            height: 19px;
            position: fixed;
            background: #B2D235 url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAlMAAAAUCAIAAAAm4S9CAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAARVJREFUeNrs281twkAQgFH8LyOCg9JBGkmD6SpNRcJrZ9BKFJATGr93oID14dPMss33z9fp5Z37be7Ludumbmub/QQA/9W/fu3i13cCIGf5YqSL4F36onYApC3f0O6XocxdiebZZAKQs3xRuMcysyvRvCifzwBAzvJZZgJwiPLVZabxDoDM5Wubx+1djHdu7wDIXL6Y6ub+EbzInsMFIG356v8zr8M6dZszBSBt+dpmX0bBAyB7+Z53eFaaACQvXw3edVwdHACZyze0+/t49ywBgOTlq1vNZfDwHIDs5atD3jIWT/EASF6+GPJu42rIAyB5+epi82O6u8kDIHn5onm3aY05z2ITgEOU7/PtV/MAOI4/AQYASFBVNsg8NDMAAAAASUVORK5CYII=') no-repeat left;
        }

        footer {
            left: 0;
            right: 0;
            bottom: 0;
            height: 19px;
            position: fixed;
            background-size: auto;
            background: #037456 url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAlMAAAAUCAIAAAAm4S9CAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAATFJREFUeNrs3L1Kw1AYgOEKOphFDF3i0OUsQhx1rKN34JV47cbGGvSjB0T8F1qbn+chQ2kChS4v3zmHHMzubmcAMAFl0S3Tw6E/AoApNO/m/P5ysYrPygfAmKX5epmai6p9/Ub5ABih46PnqF3MeadF9+6W8gEwKmcnT9epqas24vfpA8oHwHiGvGVqonzfP6l8AAxbBC+ufHrlN5QPgEGK2e5qsYrmfdzJUz4AxqMsurpqo3k/rmoqHwCTDp7yATCt4CkfAP2V9/DS/HGLwVM+AHonn9JM8/VfD60oHwCDURZdpK7eNO9/flH5ANiDqN1mvNvJeqbyAdCX8a7eLGZG8L56tZjyATBs+aVikbpd794pHwD7rF1O3V4WM5UPALVTPgC2IR/LzKnree3eehFgAMhEYDjgfx5sAAAAAElFTkSuQmCC') no-repeat right;
        }

        .header-logo {
            width: 84px;
            float: left;
            height: 71px;
            margin-left: 30px;
            background-size: cover;
            background: #007355 url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFQAAABHCAIAAAAWS/ejAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAACAJJREFUeNrsmk9MVUcUxh+IFJEaxGKJTRWriSlo1Ai1aWvBtLGpTURjrXXFM+lGbYK06kIX4MIu1GhZiJsmPlZqXSA0Na2JETRNaJ+JmoI2bVHQxigYFKto/dvfux9MhvvgAQ/wid7Jy83cuTNz5zvzzXfOuRDn+3qF70Ut8b4XuHjgPfAeeA+8B94D74H3wHvgPfAe+OejJER4VroolPCdudJ0uD5oty+dmTtnUmaow9FD/XxN/rTs42tKqMRt+GxIhuwu8K9fsJi1zd21aVjAFzggb969U9P4JVc1ZqalV/o3UqlpPOfzHYrVpsn6ug4L7YurKrimjhm7u6DQNO5buY4rtlh9cE8MGVt28kes/+3JI8NF+5rGBmaHXf6c/IpgLbcQPn9alvPuI01trf7c/ILsXKxDS1X9b4FTtSII3YoWfEK9uDpQsmjFzbsdVd0PDkP2rVyrgQv3lnLlLQUz35JZqxqCgWCN6QzXmCRz/ETqFadq9EhjVY6vKZU5aCzMyS+uCnAcIEXR+4s1iluesuABgKdsPXrIn5OnzV+4dysnjcamG62cdsgPSOuIZrFE+ujFshFXxrJFNnhaOMxi7OqD5VxPf7XdJjDTpiYln7nSrNvTxdsNVCacPSkTeJnj0/UKNTpHYIq6cWVXMK69NozLu2yb9q32zu5V6HSxYl4ZWvGBcrRQyAGGgSAIPXlr5eqN9p5QxxaQojfkrAZh0y3zLAvsYCqMC4nsITTyVLQCBlwIXyrd6IA281TIeTWvwFJMGNLIJYWugQl9HgzWB5cwnpYYOFUD/yv9G6jzJparbuytrGPTwXQAYRdFuyG3XzQlLR1TslZ+3aUnoLMNGGmtyBy+T+Ldvs/Xip7GERxuCF7cvAfrLM3OtWUioT/CgLYxuJMIVRUg0faWWRNhEd4HeLGjc93VgR5V2vEgDWphuTopKAs/HjGt7UTNcl0e171Jp2qZx5iGZTzZ+X04OwYc5CAVjmNjoc3G5/Vo+/CBPfZ0BG+dvWMQSsN5hHYoxBhQae99YdGofQQvoErRgsWmbo6u9r/3/QFkB+dWIqRd5aSgI6udirxA3rRsmTuK0nSjxefLYhlTt62zGSdeDEF4axbNIURa8QJSAUlOpBN0oBzpkgKFHFhaOpsMYAQfW0QgUf8LXlm0Z07WpkVSl6sa7M5LmeXPmNoonAjc57pDAdKBcpRP5NeGMNsca3H486jBQ0Y0EqjMaTu86GmvJRrmODhL7SDHDiSou0jrKNw5e32YL89xARXBGjxFYW6ehEqCRweWHs58tdAH7lDXerr0qMkmJvLEqdTazJyu2eK8P1d5KW1YMalrT4ra6gpRoijyw8hE6phkNAlyzt29qTfv+LTBk5yY+Dn87A0evCvy4Xxy7J8V8IPxNwMqeEfED/8c7opjBt7E7eajChQ1milfUuFEZgT/ykPpaXQbMGUnjth4TN6qR67XMYqpcPsYoraxgYFMCx1c89DCPPIUTtYULHHCQaXGwx7hGa46GWuySHv2SpM+LVldsojVZS87mdMjcgw72jXnC1TUux+3bvMQrpgIUmFizNReeOAtKtDc1qIAFsqQtOnUaFt2FxSqp1Jg+rhyjB41VT1NKK15hJxHygiGi/b9LOYscCV57CJwB3W/kwuHvi7k5LtS4Oa21pLes5eQ8u/aZESH4YojNQ/IlfaFuJA01k6inyp4J5jrVAECPud7U3qP7HClwARhEcDb6WNzl/6bjwJ25IfoxAy8+T5lAmnoKndF0hIhvYs2Xev8qudrdFs2lhGeMT90RXj5mfTOCHWRJU7+nLwBg29rtdNBcaFoEIKXMHTb0mr8GbS0dRgCo0w6t0RyEBVJj06lOe3wC0Lpy9Iw+vmBBioli0KfMdiWkp7+BBBKWidl2ilwFIVQQq+wv45IBYcLPIfN5JKuJNdsuPTZcUWdEQ5yoK/3JtU3wYlyTGFQB6NhmrbWSj+VvZpXlzofi3XU6YwpowY/wlJaBUsVwRr9gYQzX+nfQKPtRGNA+6dTkEl983F9k9p6NJq/Go7yvZPdZ6exiY8/nvHv8pntXOdP7hif/Oif9tEPHsU9ffB/tF7JGJdquzcOxbLAzrpLfw0L7Zdk3Vo1+yb47cY79+O/C6Yd+zslVhTI79KOwSSCfdB+1ZybIFe95XYCv4kpD/lhi6J3r19oS7zYlhgT8OEf5IYYPCCFnH0u++WVukvJap+VcW/zwhZuDfIPpt+e/3pHisOOusvJMIIhz76CRKI9ewsqKlt+zvj9apLLLrBAcvDNR1enpt23n2KUbccnqsNI/YD5hgMJJC7kOgLGQEIOEfafTZUK0LJlYcvI/oApVBd6P9Xs/9uTO6gAe/+Z1M5tv5H4RW4bY/nFShGGYOe1va+mPOytg3lky371uXGqyC4jFbz2fGbGPdeRDi8jQt4GBv6H85176JI01H7/qkto4bWuk08sYMcFqtRfTerTarEtkSI8eXVkL3HUE2K7WQ4FCufdWD6rnRZYTZxDI310TXnp8ZI3b306q10yOXrUk/XvXafbT3++PPLAh3bvWtL4MY8k+8Cbkf4ft+L53roJIMSrz3vtrvqAUz2x2o4T6R9Ov80QHh1rfEbdfh/gCeB/vZyMCah33I9XkIdXI+apd/wfHdjYOw/i4YKeVp8fV143ISSWcT7CHjqfbBo78oKc5754/3XtgffAe+A98B54D7wH3gPvgffAe+BHfPlfgAEAmgLWBUil+0YAAAAASUVORK5CYII=') no-repeat center center;
        }

        .header-text {
            float: left;
            font-size: 9px;
            color: #ffffff;
            margin-left: 8px;
            padding: 3px 0;
        }

        .page-number {
            float: right;
            color: #ffffff;
            font-size: 8px;
            padding: 3px 15px;
        }

        .info-left {
            width: 52%;
            float: left;
            font-size: 10px;
        }

        .info-left p {
            margin-top: 6px;
            margin-bottom: 2px;
        }

        .info-left h2 {
            margin-top: 6px;
            margin-bottom: 2px;
        }

        .info-left div {
            color: #787878;
            font-size: 9px;
        }

        .info-right {
            float: right;
            font-size: 11px;
            line-height: 1.3;
            margin-right: 15px;
            background-color: #F5F5F5;
            padding: 9px 90px 9px 9px;
        }

        .purchase {
            display: block;
            margin: 0 16px;
        }

        .table-part {
            margin: 18px;
        }

        .title {
            font-size: 10px;
            font-weight: 400;
        }

        .title-left {
            text-align: left;
        }

        .text-bold {
            font-weight: bold;
        }

        table {
            width: 100%;
            border-spacing: 0;
            border-collapse: collapse;
        }

        tbody {
            font-size: 9px;
        }

        table tr {
            page-break-inside: auto;
        }

        th, td {
            padding: 6px;
            text-align: center;
            page-break-inside: auto;
        }

        tbody tr:nth-child(even) {
            background-color: rgba(178, 210, 53, 0.1);
        }

        /* Description Column */
        tbody td:nth-child(4) {
            text-align: left;
        }

        .page-number-auto:after { content: counter(page, decimal); }

        .booking {
            max-width: 20cm;
            padding: 0 20px;
        }
    </style>
</head>

<body>
<div class="header">
    <div class="header-logo"></div>
    <div class="header-text">Where value's born and bred</div>
    <div class="page-number page-number-auto">Page </div>
</div>

<div style="margin-top: -30px">
    <div style="float: left; margin-left: 140px; font-size: 10px;">
        <p style="margin: 6px 0 0;">For the attention of</p>
        <h2 style="margin: 6px 0 0; font-weight: 400;">{{data_get($order, 'Buy_from_Vendor_Name', '')}}</h2>
        <div style="color: #787878;">{{data_get($order, 'Buy_from_Post_Code', '')}}</div>
    </div>

    <div class="info-right">
        <div class="address">
            <strong style="display:block;">DELIVERY ADDRESS:</strong><br>
            YTC Head Office<br />
            Unit 76<br />
            Kelleythorpe Industrial Estate<br />
            Driffield<br />
            <strong>YO25 9FQ</strong>
        </div>
    </div>
</div>

<div style="clear: both"></div>

<div class="purchase">
    <span>Purchase Order {{data_get($order, 'No')}}</span>
    <span style="font-size: 9px; color: #787878; display: block;">Order Date {{date('d-m-Y', strtotime(data_get($order, 'Order_Date', '')))}}</span>
</div>

<footer>
    <div class="page-number page-number-auto" style="margin-right: 40px;">Page </div>
</footer>

<div class="table-part">
    <table>
        <thead>
        <tr>
            <th style="border-bottom: #007355 2px solid;" class="title" scope="col">Vendor No</th>
            <th style="border-bottom: #007355 2px solid;" class="title" scope="col">YTC Item No</th>
            <th style="border-bottom: #007355 2px solid;" class="title" scope="col">Variant Code</th>
            <th style="border-bottom: #007355 2px solid;" class="title title-left" scope="col">Description</th>
            <th style="border-bottom: #007355 2px solid;" class="title" scope="col">Qty</th>
            <th style="border-bottom: #007355 2px solid;" class="title" scope="col">Unit Cost</th>
            <th style="border-bottom: #007355 2px solid;" class="title" scope="col">Total Cost</th>
            <th style="border-bottom: #007355 2px solid;" class="title" scope="col">Tax</th>
        </tr>
        </thead>

        <tbody>
        <?php
            $subtotal = $vat = $total = 0;
        ?>
        @foreach($lines as $line)
            <?php
                $lineSubtotal = data_get($line, 'Line_Amount');
                $vatGroup = data_get($line, 'VAT_Prod_Posting_Group');
                $vatRate = (int) str_replace('VAT', '', $vatGroup);

                $lineVat = $lineSubtotal * ($vatRate / 100);
                $lineTotal = $lineSubtotal + $lineVat;

                $vat += $lineVat;
                $subtotal += $lineSubtotal;
                $total += $lineTotal;
            ?>
            <tr>
                <td>
                    <div class="content">{{data_get($line, 'Vendor_Item_No', '-')}}</div>
                </td>
                <td>
                    <div class="content">{{data_get($line, 'No', '-')}}</div>
                </td>
                <td>{{data_get($line, 'Variant_Code', '-')}}</td>
                <td>{{data_get($line, 'Description', '-')}}</td>
                <td>{{data_get($line, 'Quantity')}}</td>
                <td>£{{number_format(data_get($line, 'Direct_Unit_Cost'), 2, '.', ',')}}</td>
                <td>£{{number_format($lineSubtotal, 2, '.', ',')}}</td>
                <td>{{$vatGroup}}</td>
            </tr>
        @endforeach
        </tbody>

        <tfoot>
            <tr>
                <td style="text-align: left;" class="title" colspan="6">End of items</td>
                <td style="text-align: right" class="title text-bold">Sub Total:</td>
                <td class="title">£{{number_format($subtotal, 2, '.', ',')}}</td>
            </tr>
            <tr>
                <td style="text-align: right" class="title text-bold" colspan="7">Taxes:</td>
                <td class="title">£{{number_format($vat, 2, '.', ',')}}</td>
            </tr>
            <tr>
                <td style="text-align: right" class="title text-bold" colspan="7">Invoice Total:</td>
                <td class="title">£{{number_format($total, 2, '.', ',')}}</td>
            </tr>
        </tfoot>
    </table>
</div>

<hr style="width: 20cm; border: 0; height: 1px; background: #cccccc;">

<div class="booking">
    <div style="font-size: 15px; padding-top: 12px;">Ready to book in?</div>

    <p style="font-size: 12px;">We’ve recently updated our booking in system to be completely paperless. Please visit the link below and follow the on screen instructions to book in your delivery.
        @if (!is_null($code))
            If you have <strong>Fast Track Delivery</strong> assigned then you may select any booking in date.
        @endif
    </p>

    <p style="font-size: 12px; font-weight: bold;">In order to book-in you will need your PO number and registered business postcode. These can be found in the document header.</p>

    <div style="display: block; margin-top: 20px; text-align: right;">
        <div style="float: left; text-align: left;">
            <a href="http://deliveries.yorkshiretrading.com/" style="background-color: #007355; display: block; padding: 10px 30px; color: #ffffff; font-size: 10px;">Start booking in</a>
        </div>

        @if (!is_null($code))
            <div style="float: right;">
                <div style="display: block; text-wrap: none;">
                    <a style="background-color: #2E95EF; display: inline-block; margin-top: 7px; padding: 10px 30px; color: #ffffff; font-size: 10px;">
                        <img style="margin-right: 4px; margin-top: 1px;" width="16" height="13" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAoAAAAIACAYAAAASfd37AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAGYZJREFUeNrs3f1V28geBmDJJ/8v6cB0YFcQU8GSCtZUkKQCoIIkFYStIE4FMRWEDuIO4q1AdyYecskHYPwlaeZ5zvHl3ru7WfhJenlHluS66qimaSbhyzC9XqT/e1IB8JB5+nodXov4qut6bizAXXXHCt8klT1FD2D3xTCWwlkohDfGAQpg26Xvn/A6Da8jmwPgIJaxCIbXv84OggJ4qNIXi97rVPyGNgFAqxaxCIbXu1AGl8YBCuCui18se+fhNTV2gE66Cq/LUAQXRgEK4LbFL57xe6v4AfSqCL5xRhAUwE3L30X48qpyfR9A38Ty9z6UwAujAAVw3eI3CV8+VK7xA+i7RXiduVkE8jLYQ/mLb/d+Vv4AshCz/HPKdiATOzsDmG7y+BheI2MFyFJ8fuBLN4lA/+3kDGAof/E5fl+UP4CsxYz/kjIf6LGtzwCGIJhWq+v9AChHvC7wyhigwAIYyl8sflNjBCjSVSiBZ8YABRVA5Q8AJRAKKoDKHwBKIBRUAJU/AJRA6Lcn3QWcbvhQ/gD41TT9jgB6YO0zgOm2/49GBsAD4nMCZ8YAGRTA9JDn+Jw/n+kLwEPiZwiPPSwaum3dt4A/Kn8ArOGo8m4R9L8Aps9/9AkfAKxr5LODodvqR8rfJHz5bEwAbOCkruu5MUD/CuDX8GVoTABsYBEK4LExQPcMHih/F8ofAFsYpt8lQMfU95S/eBFvPPvnxg8AthHvCj6u63ppFNAd950BfKv8AbADR+l3CtAhv50BTM/8+2o0AOzQsWcDQnf86QzgubEAsGN+t0CH/HQGMF37981YANiD564FhG749QzgayMBYE/8joGO+PUMoOf+AbAvngsIHTG4U/4myh8AezRMv2uArhTA4B/jAGDP/K6BDvjxFnBYlcWbPzz7D4B9WtZ1/dwYoF2DVP5Gyh8AB3DkbWDoSAEMTo0CgANRAKEjBfCFUQBwIH7nQMu+XwPYBEYBwMF++QSmAO0ZuBYDgEPzuwdaLoCVZ/8BcHh+94ACCIACCByyALoYF4BD87sHWi6AAAAUpHYHMACt/AJyJzC0xhlAAAAFEAAABRAAAAUQAAAFEAAABRAAAAUQAAAFEAAABRAAAAUQAAAFEAAABRAAAAUQAAAFEABAAQQAQAEEAEABBABAAQQAQAEEAEABBABAAQQAQAEEAEABBABAAQQAQAEEAEABBABAAQQAQAEEAFAAAQBQAAEAUAABAFAAAQBQAAEAUAABAFAAAQBQAAEAUAABAFAAAQBQAAEAUAABAFAAAQAUQAAAFEAAABRAAAAUQAAAFEAAABRAAAAUQAAAFEAAABRAAAAUQAAAFEAAAJ6ibgJjAICNzNPX6/BahtdNXddzY0EBBIAyi2EshbNQCG+MAwUQAMoSzwzOwutfZwdRAAGgPItYBMPrXSiDS+NAAQSAslyF12UoggujQAEEgPKK4BtnBDkkj4EBgHZNw+tr0zQXRsGhOAMIAN2xCK8zN4uwb84AAkB3DMPrc9M0b42CfXIGEAC6KT4/8MxzBNkHZwABoJtG1eps4KlRoAACQDmOwutjKIGvjYJd8hYwAPTDVV3XZ8bALjgDCAD9MG2a5oMxoAACgBIICiAAKIGgAAKAEggKIAAogSiAAIASiAIIACiBKIAAgBKIAggAKIEogACAEogCCAAogSiAAIASiAIIACiBKIAAgBKIAggAKIEogACAEogCCAAogSiAAKAEKoEogACgBKIAAgBKIAogAKAEogACAEogCiAAoASiAAIASiAKIACgBKIAAgBKIAogAKAEogACAEogCiAAoASiAAIASiAKIACgBKIAAgBKoAIIAKAEKoAAAEqgAggAoATm4JkRANCGOujT95tKz9SW+1EC4zY8M4p+cgYQANYrrLHsXJnETyXQmUAFEACUQCUQBRAAlEAlEAUQAJRAJRAFEACUQCUQBRAAlEAlEAUQAJRAJRAFEACUQCUQBRAAlEAlEAUQAJRAJRAFEACUQCVQAQQAlEAlUAEEAJRAJVABBACUQCVQAQQAlEAlUAEEAJRAJVABBAAlUAlUAhVAAFAClUAlUAEEACVQCUQBBAAlUAlEAQQAJVAJRAEEACVQCUQBBAAlUAlEAQQAJVAJRAEEACVQCVQAAQAlUAlUAAEAJVAJVAABACVQCVQAAQAlUAlUAAEAJVAJVAABACVQCVQAAQAlUAlUAAEAJVAJVAABACVQCVQAAUAJRAlUAAFACVQCUQABQAlUAlEAAUAJVAIVQABACVQCFUAAQAlUAhVAAEAJVAIVQABACVQCFUAAQAlUAhVAAEAJVAIVQABACVQCFUAAQAlUAhVAAEAJVAIVQABACVQCFUAAQAlUAhVAAEAJVAIVQABACVQCFUAAUAIpswQqgACgBFJYCVQAAUAJpLASqAACgBKoBBZWAhVAAFAClcDCSqACCAAogYWVQAUQAFACCyuBCiAAoAQWVgIVQABACSysBCqAAIASWFgJVAABACWwsBKoAAIASmBhJVABBACUwMJKoAIIACiBhZVABRAAUAILK4EKIACgBBZWAhVAAEAJ3L4Evu7VNgzfcGO7AdBCiahNod/Sma+pSfzwMuzWMwUQABRAJbAcy/A6Cbv2jQIIAAqgEliOm7Brj7v+TboGEADYtsy7JvD/RqEQv+38NnMGEICWSoMzgJlxJvAn8a3guQIIAAqgEliORdjFj7v6zXkLGADYZbH3dvDKMJThi85uJ2cAAWipKDgDmDFnAr+LdwUfh1192bVvzBlAAGAfBd+ZwKo6Cq9O3hDiDCAAbRUEZwAL4Ezgd/Es4KJL35AzgADAPou+M4FVdd657eIMIAAtFQNnAAviTGD1vEvXAjoDCAAcovCXfibwdae2hzOAALRUCJwBLFDBZwI79VxAZwABgEMW/1LPBMbnAk4UQABACSzLP53ZBt4CBqClEuAt4MIV+HbwMuz2z7vwjTgDCAC0tQgo7UzgUSi9IwUQAFACyyqBpwogAKAEllUCX3Ri5q4BBKClX/quAeQnpVwT2IV93xlAAKArxaiIM4FdeByMAggAKIGH1fqNIAogAKAEHtaRAggAUFYJbP1GEAUQAFACS5utu4ABaOmXu7uAWUuOdwe3vf87AwgAdL0sOROoAAIASiAKIACgBKIAAgBKIPfM0U0gALT0i9xNIGys7/3FTSAAACiAAAAogAAAKIAAACiAAAAogAAAKIAAAAogAAAKIAAACiAAAAogAAC988wIAGiDz6KH9jgDCACgAAIAoAACAKAAAgCgAAIAoAACAKAAAgCgAAIAoAACAKAAAgCgAAIAoAACAKAAAgCgAAIAKIAAACiAAAAogAAAKIAAACiAAAAogAAAKIAAACiAAAAogAAAKIAAACiAAAAogAAAKIAAACiAAAAKIAAACiAAAAogAAAKIAAACiAAAAogAAAKIAAACiAAAAogAAAKIAAACiAAAAogAAAKIACAAggAgAIIAIACCACAAggAgAIIAIACCACAAggAgAIIAIACCACAAggAgAIIAIACCACAAggAgAIIAKAAAgCgAAIAoAACAKAAAgCgAAIAoAACAKAAAgCgAAIAoAACAKAAAgCgAAIAoAACAKAAAgAogAAAKIAAACiAAAAogAAAKIAAACiAAAAogAAAKIAAACiAAAAogAAAKIAAACiAAAAogAAAKIAAAAogAAAKIAAACiAAAAogAAAKIAAACiAAAAogAAAKIAAACiAAAAogAAAKIAAACiAAAAogAABV3QTGAADQW/Pwug6vWV3XNwogAEBZYgG8DEVwpgACAJRlHl5noQgu/vQXXQMIAJCfSXh9aZrmVAEEACjHUXh9DCVw+utf8BYwAED+Xt69LlABBADI3zK8xrfXBHoLGAAgf/Ht4A+3/0MBBAAow+T2phAFEACgHK/if7gGEACgLGNnAAEAynKqAAIAlOWFt4ABAArjDCAAgAIIAEDOnhkBexSfOn6TXv+lr8tH/pn4oMpReP2Vvo7S/wcg/2BHXAPILsWAm4fXdfxa1/VyF39o2EVjAE7C60X6OjJqQP6BAkh74gdLf0qBtzjEvzDsssMUhH+H16lNAMg/UADZvxh072P4HSr0HgnDGILxyeZDmwaQf6AAslvzGHwh9GZd/ObS5xu+SqtjAPkHDxTAz3YY1gi+yxB88z58s2Gfjvvzuf0akH/wZx4Dw0MW4XUWgu+kL+H3fVUTvtf4PcfvPf0MAPIPfimA18bAH1yG1zgEyVVff4D0vY/TzwIg/+B2H2maZhq+fjAKkpu06r3J6YcK+/ko7eceoQDIP4oXzwAujIEkrhhPcgu/tBqOP9NJ+hkB5B9Fq9PqwJ3AZYsPLH3T57c7nrganoYvbytP2AfkH4UXQHcClx1+Wa56HwnB+FbIZyEI8k/+UaLbu4DdCFKmGHrj0sLv+8pn9TOP0wwA+Sf/KLIAzo2iyPA7aftJ9i2HYPzZT4QgyD/5R3H7wO1/aZrmW+V0cGnhtzSKHx+2Ht8OcYccyD/5RxHuPgh6ZhzCr9CV8NJKGOSf/KOobX9nFTBJqwDyFQ/0cclvezyyEh6GL18qZ8JB/sk/Mje4swqYV54JmHv4nQi/B1fCi7QSdnYA5J/8o4wCmPxrJNl6U+LdbhuEYJzRG5MA+Sf/yHp73/0f6WLQb8aSnatwYJ8Zw/rCsRA/NmlqEiD/5B/ZF0AbPksuet4sAN0ZB/JP/lFUARyGL1+NJhtjb31sHIIx/L6YBMg/+UduBr81wtWFoFdGk4VL4bfF6mg1u0uTAPkn/8huG9/T/OPp33gW0O3g/bVIq19vfWy3Cj5Kq+ChaYD8k3/kYnBP848HzXvj6f3qV/htvwpeWgWD/JN/ZLd9H2n/XzX/XpqHA/fEGHa6Eo4XRE9MAuSf/CMHg0f+ulvne7r6NQIzBccqZspGBTB9Osg7Y+rd6nduDLuVZmquIP/kH/kXwLTh41PB3UnVH67dNFtwjGK2PNzv1vmbfEh0byxCYT82hv1xXSzIP/lHDgbr/E3p2YCuB7RCw4zBsWnGZKB+yt8c2v80fPlgbJ11nMo6+1sBx9WvT8oB+Sf/6LXBU/7mcHBdVT4lpKtmwu8AK6bVjGcmAfJP/lFMAUw7wJkS2EmfjMCswTGJWbNWn9v0H2yaJr4VPDXCznjuyfeH4W0QkH/yj74bbPoPOhPYKTfC74CrptXbIB6NBPJP/tFX88GWO0Isge4O7sCGNAIzB8ciZs6argfb/gnpxpCX4WUF1uKGNAIzB8ciZs6aZvWu/qR0XcDH8BqZ68G5/uXAwv4eH4r+zSRA/sk/eiZeNjEe7OpPi9cFxD+w8tnBh7YUfoeXZm7uIP/kH31zGf9jsIcdI3528El4Lcz4ME3eCMweHIOYPWuYh54220sBTCVwnj6T8dIqwUFo9oBj0OxpXexjP27cHezz3xRK4EX4EovglbnvzX9GYPbgGMTsecTZ3U/MGez73xavFUiPi1EErcLMHnAMmj3tlL+fPsZvcKh/c7pJJBbB59XqreGF7bET3mI3e3AMYvbct51epkf2Va0UwDtFMJ4RvEjXCMabRa7sSAAAOzUPr/GvZ/5aK4C/lMF4s0g8LRnPCsZHyFxWnjAOALBN8Ytn/U7uXvP3q2dd+W7DNxmvJ/hxTUHTNJPwZZheL9L/PbFdAQB+K33xE1pmqU896llXf5J4dvAQ/55QNBv7DQXzKQoFk3/Iv3Lzb2D7Q7mUP0D+KYAAACiAAAAogHTNkRGYPTgGMXsUwLKMjMDswTGI2aMAluUvIzB7cAxi9iiATzO3CqPQ2c9tQvnnGET+KYA4CDF7cAxi9gogPXDUNI2LcQ8szdzcQf7JPxTAnrrO4GeY2IxmXui+j31A/pm5/FMAi/XCCMwcHIuYOQrg+m4y+BmsgM281H0f+4D8M3P5pwBuJIfPAhw1TTO0KQ8jzXpk38c+IP/kn31fAeypuq7nVmSUOOuM9n3sA/LPrOWfAljsSuBvm9GsrX6Rf5i1/FMA15fDtQCn3gbZvzTjU/s88k/+yT/7vALYf7ncDn5qU5pxYfs89gX5Z8byTwHc2CKTn+OVTWnGhe3z2BfknxnLPwVwY7mcDh42TWMVvCdptkP7PPJP/sk/+3zf1UbwY+f+VuXx0Tbzuq5PbNG97COfqzzugFuGfeS5LYr8Q/6VyxnA/FYEk3CgTmzOnYffpMrnURNWv8g/5J8CSJLTRaHnNqeZFrKvY5+Qf2Yq/xTArcwz+lniKnhqk+5s9Tut8nrQ7NxWRf4h/8rmGsCfd/Qmox9nEV7juq498HK7fSJeF/Wlyufi5/gEfMc98g/5VzhnAPNdGcQD9rVNurXXOYWf1S/yD/mHAvi7T5n9POdhBTeyWTde/cbZndvHkX/yT/7ZxxVAK+C++ZBO4/O08Isz+5DhjzazdZF/yD8UwDvquo63hy8y+7HiKu6trftkb9PscrII+/jCpkX+If9QAMtYIUzdFfek1W+c1dS+jfyTf/LPvp3tos8Iftv546rnS4Y/Wrwb7iSt8nl4+8cn3uf4ttHY9kf+If9QAO8/CL5Wed35dDcEx06D37vdh+mXX47hF9/+OLaVkX/yT/4ReQv4z3I9VRwP7I8uiv5j+H2fTabhl/M+jX1F/sk/+bcBZwDvXwl9zfhHjKfBTzwk9afwi2975PzIiGNnPpB/8k/+ccsZwD+14tWOkvO1At+v87ASLib8boQf8k/+yT8UwPW8z/znuw3BYcHhNywg/ErYl7HPyD/5J/+eutgzggdXRvFtkNxXiUXeHZf53W6/bt9jb3ch/+Sf/OMuZwDva8arHeaqgB/1KK2EpwWF37SQ8IuuhB/yT/7JP347zo3gwQNlWOV9MfRvB0t4vcn1gElnNeIT7qcFbVMXPyP/5J/84zfOAD68Co47Tkm3j0/Taji7a0LuvOVRUvjNhB/yT/7JPxTAzZR2Aen3TwIIgXGRw11y8WeIP0u1esDpyL4L9iH5Z9/FW8DrHkRx5TQp8EePq6fLsIq66ul2i6vd8yrPTzV4zDxstxNHL/JP/sk/FMDND6QYfp8LHsE8BeG8R9vrvNBfWrdO+rK9kH/yT/7JPwXQKrj7Qfg+HFizjm6j0/Dlle1k9Yv8k3/yDwXQKnj3FtXq+orWL7JNdyreBt/QprH6Rf7JP/lnDAqgVfD+xdXwp7TqWhxoOwzTdvg7hR9Wv8g/+Yf8UwD3duB9v0PMJO4Vn6YfV17X6UBc7mjuRynwXqSvI6O+17i0TzVA/sk/5J8CeIgQ/FCV9SylbSxTKMbXf+nr8g9/z93HLRylgPsrfR1VZTyxfhfiU+/PjAH5J//kHwrg7gNwmFbBDkq69stm7MGnyD/kH+vwIOinNubVDuYBk3TNe+GH/EP+sfbxbAQbr4RLfLI63XQTwm9sDMg/5B/rcgZwc2+MAPsi9jmwLyqABUnPGXpnErTsnWdeIf+Qfzz5ODaCzaXb8+NbIUPToAWLanXh89IokH/IP57CGcDtVsFxx3PbOW05E37IP+QfCmA7ITivvBXC4XnrA/mH/GPz49cIdsNdcRyQu96Qf8g/tuIM4O68rH5/yjvs2jLtayD/kH8ogG1LD6F0PQz7duaBp8g/5B8KYLdCcFa5Hob9eZf2MZB/yD+2O2aNYPeapvkcvkxMgh2ah/A7MQbkH/KPXXAGcD/iNQo3xsCO3FSue0H+If/YIWcA97cKjnfExZXwkWmwhXjR80lY/fqFivxD/qEA9igEv5gEWxgLP+Qf8o9d8xbwPtv1asd1ZxybOhN+yD/kH3s5Ro3gICvhafjywSR4YvhdGQPyD/mHAigEEX4g/5B/KIBCEOEH8g/5hwIoBBF+IP+QfyiAQhDhB/IP+YcCKAQRfiD/kH8KIEIQ4QfyD/mnAHKIEPTE/DJ5wj3yT/7JP1rjQdBtN/DVARA/5NqBUI4b4QfyT/7R6vFnBJ1ZCccV8MfwmphG1ubh9TKE39IoQP7JP9riDGB3VsLL8Ior4Xemka13cRsLP5B/8o/Wjzsj6ORq+LRaXRztupg8xMCLFzvPjALkn/xDAeShEBxWq7dERqbRa/E6l/iWx8IoQP7JP7rCW8BdbebhgAmvceUtkT6Lb3mMhR/IP/lH544zI+jFanhSrd4SGZpGL8TAi295zI0C5J/8o4ucAezHajgeSFbDPVn1xm0l/ED+yT86fWwZQS9Xw28r18Z0TbzW5Y3gA/kn/1AA2WcQXoQvryp3yrUt3uH2PgTfhVGA/JN/KIAcIgSH4ct5eE1NoxVX4XXpImeQf/IPBZA2gjC+HRLfFpmYxkHMq9XbHT7KCOSf/KOX3ASSQ4sPB2J6iv5JOjjZX/CdpKfZCz+Qf/KP/h47RpDlijiuhM+tiHcafJcucAb5J/9QAOlLEMYLpU9NYyPxo4veCz6Qf/IPBZA+BuEwBeG0ctfcY+JdbVcp+BbGAfJP/qEA0vcgPEqr4RiGnqP1s3hNy/u46g3BtzQOkH/yDwWQnFfFMRCHhY4hrnBnVrsg/+QfCiAlhmFcDf9TSBjeht6/7mQD5B8KIPx/ZRyD8O8qn7vo5uH1qVq9vWGlC8g/FEAj4JFAnKQgfFGtrpvp+kXU8fqVuLK9jsHnDjZA/oECyPaBOEpBOOxAKN4Nu7iyvfG2BiD/QAHksCvlo+r/d9e9uPOXJxv+sXdXr9fpawy4pZUtIP9gc/8TYAAKGErIeclzBwAAAABJRU5ErkJggg==" />

                        Fast Track Delivery</a><span style="border: 1px solid #2E95EF; border-left: none; display: inline-block; padding: 10px 30px; color: #2E95EF; line-height: 12px; font-size: 10px;">{{$code->code}}</span>
                </div>
            </div>
        @endif
    </div>

    <br style="clear: both;" />

    <div style="display: block; font-size: 9px; padding: 16px 30px 10px 0;">
        Link not working? Try:
        <a target="blank" href="http://deliveries.yorkshiretrading.com" style="color: #007355; text-decoration: none;">http://deliveries.yorkshiretrading.com</a>
    </div>

    <hr style=" border: 0; border-bottom: 1px dashed #ccc;">
</div>
</body>
</html>
