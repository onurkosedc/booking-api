@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                <div class="card">
                    <div class="card-header">
                        Deliveries
                        <div class="float-right">
                            <a href="/deliveries/fake-some/5" type="button" class="btn btn-primary btn-sm">{{session('status') ? 'Fake some more' : 'Fake some data'}}</a>
                        </div>
                    </div>

                    <table class="table table-hover" width="100%">
                        <thead>
                        <tr>
                            <th scope="col">&nbsp;</th>
                            <th scope="col">Order ID</th>
                            <th scope="col">Email</th>
                            <th scope="col" class="text-right">Delivery date</th>
                            <th scope="col" class="text-right">Created at</th>
                            <th scope="col">&nbsp;</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach ($deliveries as $delivery)
                            <tr>
                                <td>
                                    <div class="form-check">
                                        <input class="form-check-input position-static" type="checkbox" id="blankCheckbox" value="option1" aria-label="...">
                                    </div>
                                </td>
                                <td>{{$delivery->order_id}}</td>
                                <td>{{$delivery->email}}</td>
                                <td align="right">{{$delivery->delivery_date->format(Auth::user()->settings()->get('date_format'))}}</td>
                                <td align="right">{{$delivery->created_at->format(Auth::user()->settings()->get('date_format'))}}</td>
                                <td>
                                    <delivery-listing-actions :delivery-id="{{$delivery->id}}"></delivery-listing-actions>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <div class="card-footer align-items-center">
                        {{$deliveries->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
