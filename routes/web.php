<?php

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\ValidationException;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::post('/sanctum/token', 'SanctumController@token');

Route::get('/{pattern?}', 'WelcomeController@home')->where('pattern', '.*');


/*
Route::get('/', function () {
    return view('welcome');quas
});

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/deliveries', 'DeliveryController@index')->name('deliveries');
Route::get('/deliveries/fake-some/{id}', 'DeliveryController@fakeSome')->name('deliveries-faker');
Route::get('/units', 'UnitController@index')->name('units');

Route::get('orders/{uuid}', 'OrderController@show');
*/
