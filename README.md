# Booking-in System API

Laravel/GraphQL API

## Install the dependencies
```bash
composer install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
php artisan migrate
php artisan serve
```
