<?php

namespace App\Http\Controllers;

use App\Models\Delivery;
use App\Models\Unit;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use ODataQueryBuilder\ODataQueryBuilder;

class UnitController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $units = Unit::get();

        return view('unit', compact('units'));
    }

    public function list(Request $request)
    {
        $units = Unit::orderBy($request->input('sortBy', 'id'), $request->input('sortDesc', 'true') === 'true' ? 'desc' : 'asc')->get();

        return $units;
    }

}
