<?php

namespace App\Http\Controllers;

use App\Models\Delivery;
use Illuminate\Http\Request;

class DeliveryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $deliveries = Delivery::simplePaginate(28);

        return view('delivery', compact('deliveries'));
    }

    public function fakeSome(Request $request, $amount = 3)
    {
        factory(Delivery::class, (integer) $amount)->create();

        $request->session()->flash('status', $amount . ' fake deliveries created!');

        return redirect()->route('deliveries');
    }
}
