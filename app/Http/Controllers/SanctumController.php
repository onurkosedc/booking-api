<?php

namespace App\Http\Controllers;

use App\Models\User;

use Hash;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class SanctumController extends Controller
{
    public function __construct()
    {
    }

    public function token(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
            'device_name' => 'required'
        ]);

        $user = User::where('email', $request->email)->first();

        if (!$user || !Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages([
                'email' => ['The provided credentials are incorrect.'],
            ]);
        }

        return $user->createToken($request->device_name)->plainTextToken;
    }

}
