<?php

namespace App\Http\Controllers;

use App\Models\Delivery;
use App\Models\Order;
use Exception;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use ODataQueryBuilder\ODataQueryBuilder;

class OrderController extends Controller
{
    public function __construct()
    {
    }

    /**
     * @param $url
     * @param null $status
     * @param int $wait
     * @return mixed
     */
    private function http_response($url, $status = null, $wait = 3)
    {
        $odataURL = "http://83.100.144.178:7048/LIVE/OData/Company('YTC%20LIVE')";

        $url = $odataURL . preg_replace('/\?/', '?$format=json&', $url, 1);

        $time = microtime(true);
        $expire = $time + $wait;

        $username = "YTCNET\webservice";
        $password = 'RUbttjt8WcPttUneT8YDTuU';

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_GSSAPI_DELEGATION, CURLGSSAPI_DELEGATION_FLAG);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_NTLM | CURLAUTH_GSSNEGOTIATE);
        curl_setopt($ch, CURLOPT_USERPWD, $username.":".$password);
        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $json = json_decode($result, true);
        curl_close($ch);
        return $json;
    }

    /**
     *
     */
    public function index()
    {
        return abort(401);
    }


    /**
     * Web endpoint
     *
     * @param Request $request
     * @param $uuid
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws Exception
     */
    public function show(Request $request, $uuid)
    {
        $visibleOrder = $request->session()->get('visible_order', null);

        abort_if(is_null($visibleOrder) || $visibleOrder !== $uuid, 404);

        $orderModel = Order::findByUuid($uuid);

        if (is_null($orderModel)) {
            abort(404);
        }

        $orderListingQuery = $this->getBuilder()->from('Purchase_Order_Lines')->filterWhere('Document_No')->equals($orderModel->order_number)->buildQuery();

        $orderListingRaw = $this->http_response($orderListingQuery);

        $orderListing = $orderListingRaw['value'];

        return view('pages.orders.show', compact('orderModel', 'orderListing'));
    }

    /**
     * API endpoint
     *
     * @param Request $request
     * @return mixed
     * @throws Exception
     */
    public function lookup(Request $request)
    {
        $orderHeadersQuery = $this->getBuilder()->from('Purchase_Order_Headers')->filterWhere('No')->equals($request->input('purchase_order_number'))->filterWhere('Buy_from_Post_Code')->equals($request->input('postcode'))->buildQuery();

        $orderHeaders = $this->http_response($orderHeadersQuery);

        if (count($orderHeaders['value'])) {
            $order = $orderHeaders['value'][0];

            $orderModel = Order::firstOrCreate([
                'order_number' => $order['No'],
                'postcode' => $order['Buy_from_Post_Code'],
                'email' => $request->input('email')
            ]);

            /*
             * Assign order uuid to visible_order key then
             * confirm the key on order lines page
            */
            $request->session()->put('visible_order', $orderModel->uuid);

            return $orderModel;
        } else {
            abort(404, 'order_not_found');
        }
    }

    /**
     * @return ODataQueryBuilder
     */
    private function getBuilder(): ODataQueryBuilder
    {
        return new ODataQueryBuilder('');
    }
}
