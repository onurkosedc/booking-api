<?php

namespace App\Http\Controllers;

use App\Models\Delivery;
use App\Models\Unit;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use ODataQueryBuilder\ODataQueryBuilder;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function self(Request $request)
    {
        return $request->user();
    }

}
