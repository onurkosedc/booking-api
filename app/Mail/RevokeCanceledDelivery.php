<?php

namespace App\Mail;

use App\Mail\Concerns\FromAddressResolver;
use App\Models\Delivery;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RevokeCanceledDelivery extends Mailable
{
    use Queueable, SerializesModels, FromAddressResolver;

    public $delivery;

    /**
     * Create a new message instance.
     *
     * @param Delivery $delivery
     */
    public function __construct(Delivery $delivery)
    {
        $this->delivery = $delivery;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $from = $this->resolveFromAddress($this->delivery->domain);

        return $this->from($from)
            ->with([
                'delivery' => $this->delivery,
            ])
            ->subject('Delivery Slot Update Notification')
            ->view('emails.revoke-canceled-delivery-' . $this->delivery->domain);
    }
}
