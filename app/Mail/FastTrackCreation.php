<?php

namespace App\Mail;

use App\Models\Delivery;
use App\Models\FastTrack;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FastTrackCreation extends Mailable
{
    use Queueable, SerializesModels;

    public $code;

    /**
     * Create a new message instance.
     *
     * @param FastTrack $code
     */
    public function __construct(FastTrack $code)
    {
        $this->code = $code;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('booking-in@yorkshiretrading.com')
            ->with([
                'code' => $this->code
            ])
            ->subject('Fast Track Code')
            ->view('emails.fast-track-creation');
    }
}
