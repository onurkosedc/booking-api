<?php

namespace App\Mail;

use App\Mail\Concerns\FromAddressResolver;
use App\Models\Delivery;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RejectedReservation extends Mailable
{
    use Queueable, SerializesModels, FromAddressResolver;

    public $delivery;
    public $reason;

    /**
     * Create a new message instance.
     *
     * @param Delivery $delivery
     * @param $reason
     */
    public function __construct(Delivery $delivery, $reason)
    {
        $this->delivery = $delivery;
        $this->reason = $reason;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $from = $this->resolveFromAddress($this->delivery->domain);

        return $this->from($from)
            ->with([
                'delivery' => $this->delivery,
                'reason' => $this->reason
            ])
            ->subject('Cancellation of Delivery Slot Notification')
            ->view('emails.rejected-reservation-' . $this->delivery->domain);
    }
}
