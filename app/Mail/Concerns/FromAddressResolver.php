<?php

namespace App\Mail\Concerns;

trait FromAddressResolver
{
    /**
     * @param string $domainIdentifier
     * @return string
     */
    public function resolveFromAddress (string $domainIdentifier): string
    {
        $from = 'ytc';

        switch ($domainIdentifier) {
            case 'ytc':
                $from = 'booking-in@yorkshiretrading.com';
                break;
            case 'tf':
                $from = 'inboundgoods@totalfulfilment.co.uk';
                break;
        }

        return $from;
    }
}
