<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static updateOrCreate(array $controlData, array $array)
 * @method static where(array $array)
 */
class OrderLine extends Model
{
    protected $table = 'orders__lines';

    public $timestamps = false;

    protected $guarded = [];
}
