<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Order extends Model
{
    use Uuid;

    protected $table = 'orders';

    protected $uuidColumnName = 'uuid';

    protected $primaryKey = 'uuid';

    protected $keyType = 'string';

    public $incrementing = false;

    protected $guarded = [];

    protected $dates = ['last_access'];

    public function user(): BelongsTo
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function lines(): HasMany
    {
        return $this->hasMany('App\Models\OrderLine');
    }

    public function deliveries(): HasMany
    {
        return $this->hasMany('App\Models\Delivery');
    }
}
