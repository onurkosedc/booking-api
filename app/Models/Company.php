<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Glorand\Model\Settings\Traits\HasSettingsField;

/**
 * @method static firstOrCreate(string[] $array)
 */
class Company extends Model
{
    use HasSettingsField;

    protected $table = 'companies';

    public $settingsFieldName = 'settings';

    public $defaultSettings = [
        'can_cancel_after' => '01:00',
        'cannot_cancel_before' => '01:00',
        'branding_colour' => '',
        'logo' => '',
        'splash' => '',
    ];

    protected $guarded = [];
}
