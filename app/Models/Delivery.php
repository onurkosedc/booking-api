<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property string domain
 * @method static create(array $array)
 */
class Delivery extends Model
{
    use Uuid;

    protected $table = 'deliveries';

    protected $uuidColumnName = 'uuid';

    protected $primaryKey = 'uuid';

    protected $keyType = 'string';

    public $incrementing = false;

    protected $guarded = [];

    protected $dates = ['delivery_date', 'canceled_at', 'confirmation_changed_at'];

    public function units (): HasMany
    {
        return $this->hasMany('App\Models\DeliveryUnit', 'delivery_uuid', 'uuid');
    }

    public function order_lines (): HasMany
    {
        return $this->hasMany('App\Models\OrderLine', 'delivery_uuid', 'uuid');
    }

    public function order (): BelongsTo
    {
        return $this->belongsTo(Order::class);
    }
}
