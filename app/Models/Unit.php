<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Unit extends Model
{
    protected $table = 'units';

    protected $guarded = [];

    //protected $dates = ['delivery_date', 'canceled_at'];

    public function schedule(): HasMany
    {
        return $this->hasMany('App\Models\Schedule', 'unit_id');
    }

    public static function boot() {
        parent::boot();

        static::deleting(function ($unit) {
            $unit->schedule()->delete();
        });
    }
}
