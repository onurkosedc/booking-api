<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @method static create(array $array)
 * @property int id
 */
class FastTrack extends Model
{
    protected $table = 'fast_track';

    protected $guarded = [];

    //protected $dates = ['delivery_date', 'canceled_at'];

    public function delivery(): HasMany
    {
        return $this->hasMany('App\Models\Delivery', 'code_id');
    }
}
