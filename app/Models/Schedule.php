<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $table = 'units__schedule';

    protected $guarded = [];

    protected $dates = ['from_date', 'to_date'];
}
