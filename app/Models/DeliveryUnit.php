<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @method static updateOrCreate(array $array, array $array1)
 * @method static where(string $string, $uuid)
 */
class DeliveryUnit extends Model
{
    protected $table = 'deliveries__units';

    public $timestamps = false;

    protected $guarded = [];

    protected $with = ['unit'];

    public function unit (): BelongsTo
    {
        return $this->belongsTo('App\Models\Unit');
    }

    public function delivery (): BelongsTo
    {
        return $this->belongsTo('App\Models\Delivery');
    }
}
