<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BusinessHour extends Model
{
    protected $table = 'business_hours';

    protected $guarded = [];

    //protected $dates = ['delivery_date', 'canceled_at'];
}
