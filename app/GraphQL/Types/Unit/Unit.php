<?php
namespace App\GraphQL\Types\Unit;

use GraphQL;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class Unit extends GraphQLType
{
    protected $attributes = [
        'name' => 'Unit',
        'description' => 'A type',
        'model' => \App\Models\Unit::class
    ];

    public function fields(): array
    {
        return [
            'id' => [ 'type' => Type::nonNull(Type::int()), 'description' => '' ],
            'code' => [ 'type' => Type::string(), 'description' => '' ],
            'name' => [ 'type' => Type::string(), 'description' => '' ],

            'schedule' => [
                'type' => Type::listOf(GraphQL::type('unit_schedule')),
                'description' => '',
                'args' => [
                    'stillEffective' => ['name' => 'stillEffective', 'type' => Type::boolean()],
                ],
                'query' => function(array $args, $query) {
                    isset($args['stillEffective']) || $args['stillEffective'] = false;

                    return $args['stillEffective'] ? $query->orWhereDate('from_date', '>=', new \DateTime('now'))->orWhereDate('to_date', '<=', new \DateTime('now'))->orWhereNull('from_date') : $query;
                }
            ],

            'created_at' => [ 'type' => Type::string(), 'description' => '' ],
            'updated_at' => [ 'type' => Type::string(), 'description' => '' ],
        ];
    }
}
