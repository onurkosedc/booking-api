<?php
namespace App\GraphQL\Types\Unit\Schedule;

use GraphQL;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class Schedule extends GraphQLType
{
    protected $attributes = [
        'name' => 'Schedule',
        'description' => 'A type',
        'model' => \App\Models\Schedule::class
    ];

    public function fields(): array
    {
        return [
            'id' => [ 'type' => Type::nonNull(Type::int()), 'description' => '' ],
            'unit_id' => [ 'type' => Type::int(), 'description' => '' ],
            'period' => [ 'type' => Type::int(), 'description' => '' ],
            'from_time' => [ 'type' => Type::string(), 'description' => '' ],
            'to_time' => [ 'type' => Type::string(), 'description' => '' ],
            'from_date' => [ 'type' => Type::string(), 'description' => '' ],
            'to_date' => [ 'type' => Type::string(), 'description' => '' ],

            'unit' => [ 'type' => GraphQL::type('unit'), 'description' => '' ],

            'created_at' => [ 'type' => Type::string(), 'description' => '' ],
            'updated_at' => [ 'type' => Type::string(), 'description' => '' ],
        ];
    }

    public function resolveFromTimeField ($root) {
        return substr($root->from_time, 0, 5);
    }

    public function resolveToTimeField ($root) {
        return substr($root->to_time, 0, 5);
    }

    public function resolveFromDateField ($root) {
        return is_null($root->from_date) ? null : $root->from_date->format('Y-m-d');
    }

    public function resolveToDateField ($root) {
        return is_null($root->to_date) ? null : $root->to_date->format('Y-m-d');
    }
}
