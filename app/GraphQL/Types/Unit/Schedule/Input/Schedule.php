<?php
namespace App\GraphQL\Types\Unit\Schedule\Input;

use GraphQL;
use GraphQL\Type\Definition\Type;

use Rebing\GraphQL\Support\Type as GraphQLType;

class Schedule extends GraphQLType
{
    protected $inputObject = true;

    protected $attributes = [
        'name' => 'ScheduleInput',
        'description' => 'A type of'
    ];

    public function fields(): array
    {
        return [
            'id' => ['type' => Type::int(), 'description' => ''],
            'unitId' => ['type' => Type::int(), 'description' => ''],
            'period' => ['type' => Type::int(), 'description' => ''],
            'fromTime' => ['type' => Type::string(), 'description' => ''],
            'toTime' => ['type' => Type::string(), 'description' => ''],
            'fromDate' => ['type' => Type::string(), 'description' => ''],
            'toDate' => ['type' => Type::string(), 'description' => ''],

            '__typename' => ['type' => Type::string(), 'description' => 'ignore'],
        ];
    }
}
