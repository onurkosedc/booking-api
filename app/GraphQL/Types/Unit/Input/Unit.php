<?php
namespace App\GraphQL\Types\Unit\Input;

use GraphQL;
use GraphQL\Type\Definition\Type;

use Rebing\GraphQL\Support\Type as GraphQLType;

class Unit extends GraphQLType
{
    protected $inputObject = true;

    protected $attributes = [
        'name' => 'UnitInput',
        'description' => 'A type of'
    ];

    public function fields(): array
    {
        return [
            'id' => ['type' => Type::int(), 'description' => ''],
            'code' => ['type' => Type::string(), 'description' => ''],
            'name' => ['type' => Type::string(), 'description' => ''],
            'quantity' => ['type' => Type::int(), 'description' => ''],
            'processing_time' => ['type' => Type::int(), 'description' => ''],

            '__typename' => ['type' => Type::string(), 'description' => 'ignore'],
        ];
    }
}
