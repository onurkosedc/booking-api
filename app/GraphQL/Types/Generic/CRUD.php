<?php
namespace App\GraphQL\Types\Generic;

use GraphQL;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class CRUD extends GraphQLType
{
    protected $attributes = [
        'name' => 'CRUD',
        'description' => 'A type',
    ];

    public function fields(): array
    {
        return [
            'is_ok' => ['type' => Type::boolean()],
            'is_deleted' => ['type' => Type::boolean()],
            'is_updated' => ['type' => Type::boolean()],
            'is_added' => ['type' => Type::boolean()],
            'is_removed' => ['type' => Type::boolean()],
            'is_kept' => ['type' => Type::boolean()],
            'is_imported' => ['type' => Type::boolean()],
            'is_activated' => ['type' => Type::boolean()],
            'is_sent' => ['type' => Type::boolean()],
            'file_url' => ['type' => Type::string()],
            'messages' => ['type' => Type::listOf(GraphQL::type('generic_message'))],
            'message' => ['type' => GraphQL::type('generic_message')],
        ];
    }
}
