<?php
namespace App\GraphQL\Types\Generic;

use GraphQL;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class Message extends GraphQLType
{
    protected $attributes = [
        'name' => 'Message',
        'description' => 'A type',
    ];

    public function fields(): array
    {
        return [
            'message' => ['type' => Type::string()]
        ];
    }
}
