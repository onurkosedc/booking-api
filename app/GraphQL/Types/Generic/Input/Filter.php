<?php
namespace App\GraphQL\Types\Generic\Input;

use GraphQL\Type\Definition\Type;

use Rebing\GraphQL\Support\Type as GraphQLType;

class Filter extends GraphQLType
{
    protected $inputObject = true;

    protected $attributes = ['name' => 'FilterInput', 'description' => 'A type'];

    public function fields(): array
    {
        return [
            'field' => ['type' => Type::string(), 'description' => ''],
            'value' => ['type' => Type::string(), 'description' => ''],
            'values' => ['type' => Type::listOf(Type::string()), 'description' => ''],

            '__typename' => ['type' => Type::string(), 'description' => 'ignore'],
        ];
    }
}
