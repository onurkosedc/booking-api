<?php
namespace App\GraphQL\Types\BusinessHour\Input;

use GraphQL;
use GraphQL\Type\Definition\Type;

use Rebing\GraphQL\Support\Type as GraphQLType;

class BusinessHour extends GraphQLType
{
    protected $inputObject = true;

    protected $attributes = [
        'name' => 'BusinessHourInput',
        'description' => 'A type of'
    ];

    public function fields(): array
    {
        return [
            'id' => ['type' => Type::int(), 'description' => ''],
            'date' => ['type' => Type::string(), 'description' => ''],
            'day' => ['type' => Type::int(), 'description' => ''],
            'on' => ['type' => Type::boolean(), 'description' => ''],
            'start_at' => ['type' => Type::string(), 'description' => ''],
            'end_at' => ['type' => Type::string(), 'description' => ''],
            'created_at' => ['type' => Type::string(), 'description' => ''],
            'updated_at' => ['type' => Type::string(), 'description' => ''],
            'created_by' => ['type' => Type::int(), 'description' => ''],

            '__typename' => ['type' => Type::string(), 'description' => 'ignore'],
        ];
    }
}
