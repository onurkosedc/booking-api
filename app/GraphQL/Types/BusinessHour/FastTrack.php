<?php
namespace App\GraphQL\Types\FastTrack;

use GraphQL;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class FastTrack extends GraphQLType
{
    protected $attributes = [
        'name' => 'FastTrack',
        'description' => 'A type',
        'model' => \App\Models\FastTrack::class
    ];

    public function fields(): array
    {
        return [
            'id' => [ 'type' => Type::nonNull(Type::int()), 'description' => '' ],
            'email' => [ 'type' => Type::string(), 'description' => '' ],
            'code' => [ 'type' => Type::string(), 'description' => '' ],

            'created_at' => [ 'type' => Type::string(), 'description' => '' ],
            'updated_at' => [ 'type' => Type::string(), 'description' => '' ],
        ];
    }
}
