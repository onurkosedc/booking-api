<?php
namespace App\GraphQL\Types\BusinessHour;

use GraphQL;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class BusinessHour extends GraphQLType
{
    protected $attributes = [
        'name' => 'BusinessHour',
        'description' => 'A type',
        'model' => \App\Models\BusinessHour::class
    ];

    public function fields(): array
    {
        return [
            'id' => [ 'type' => Type::nonNull(Type::int()), 'description' => '' ],
            'date' => [ 'type' => Type::string(), 'description' => '' ],
            'day' => [ 'type' => Type::int(), 'description' => '' ],
            'on' => ['type' => Type::boolean(), 'description' => ''],
            'start_at' => [ 'type' => Type::string(), 'description' => '' ],
            'end_at' => [ 'type' => Type::string(), 'description' => '' ],

            'created_at' => [ 'type' => Type::string(), 'description' => '' ],
            'updated_at' => [ 'type' => Type::string(), 'description' => '' ],
        ];
    }
}
