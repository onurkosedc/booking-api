<?php
namespace App\GraphQL\Types\User\Input;

use GraphQL;
use GraphQL\Type\Definition\Type;

use Rebing\GraphQL\Support\Type as GraphQLType;

class Setting extends GraphQLType
{
    protected $inputObject = true;

    protected $attributes = [
        'name' => 'UserSettingInput',
        'description' => 'A type of input setting'
    ];

    public function fields():array
    {
        return [
            'keys' => ['type' => Type::listOf(Type::string()), 'description' => ''],
            'value' => [ 'type' => Type::string(), 'description' => '' ],

            '__typename' => ['type' => Type::string(),'description' => 'ignore']
        ];
    }
}


