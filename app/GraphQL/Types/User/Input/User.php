<?php
namespace App\GraphQL\Types\User\Input;

use GraphQL;
use GraphQL\Type\Definition\Type;

use Rebing\GraphQL\Support\Type as GraphQLType;

class User extends GraphQLType
{
    protected $inputObject = true;

    protected $attributes = [
        'name' => 'UserInput',
        'description' => 'A type of input contacts'
    ];

    public function fields(): array
    {
        return [
            'id' => ['type' => Type::int(), 'description' => ''],

            'name' => ['type' => Type::string(), 'description' => ''],
            'is_admin' => ['type' => Type::boolean(), 'description' => ''],
            'email' => ['type' => Type::string(), 'description' => ''],
            'current_password' => ['type' => Type::string(), 'description' => ''],
            'new_password' => ['type' => Type::string(), 'description' => ''],
            'password' => ['type' => Type::string(), 'description' => ''],

            '__typename' => ['type' => Type::string(), 'description' => 'ignore'],
        ];
    }
}
