<?php
namespace App\GraphQL\Types\User;

use App\Models\User as UserModel;

use GraphQL;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class User extends GraphQLType
{
    protected $attributes = [
        'name' => 'User',
        'description' => 'A type',
        'model' => UserModel::class,
    ];

    public function fields(): array
    {
        return [
            'id' => [ 'type' => Type::nonNull(Type::int()), 'description' => '' ],
            'name' => [ 'type' => Type::string(), 'description' => '' ],
            'email' => [ 'type' => Type::string(), 'description' => '' ],
            'username' => [ 'type' => Type::string(), 'description' => '' ],
            'email_verified' => [ 'type' => Type::boolean(), 'description' => '' ],
            'is_admin' => [ 'type' => Type::boolean(), 'description' => '' ],
            'status' => [ 'type' => Type::string(), 'description' => '' ],

            'created_at' => [ 'type' => Type::string(), 'description' => '' ],
            'updated_at' => [ 'type' => Type::string(), 'description' => '' ],
            'settings' => [ 'type' => Type::listOf(GraphQL::type('user_setting_group')), 'description' => '' ],
        ];
    }

    protected function resolveSettingsField($root, $args)
    {
        $settings = collect($root->settings()->get())->map(function ($value, $field) {
            if (gettype($value) != 'array'){
                return [
                    'field' => $field,
                    'value' => $value
                ];
            }

            return [
                'field' => $field,
                'children' => collect($value)->map(function ($value, $field) {
                    if (gettype($value) != 'array'){
                        return [
                            'field' => $field,
                            'value' => $value
                        ];
                    }

                    return [
                        'field' => "$field",
                        'children' => collect($value)->map(function ($value, $field) {
                            return [
                                'field' => $field,
                                'value' => $value
                            ];
                        })
                    ];
                })
            ];
        });

        return $settings;
    }

    protected function resolvePasswordExistsField($root, $args)
    {
        return $root->password != null;
    }

    protected function resolveEmailField($root, $args)
    {
        return strtolower($root->email);
    }

    protected function resolveUsernameField($root, $args)
    {
        return strtolower($root->username);
    }

    protected function resolveAvatarField($root, $args)
    {
        // $account = $root->social_accounts()->where('is_active_avatar', true)->first();

        return is_null($root->avatar) || $root->avatar === '' ? ($root->salutation == 'm' || $root->salutation == null ? '/statics/male-avatar.png' : '/statics/female-avatar.png') : $root->avatar;
    }

    protected function resolveUnreadPostsField($root, $args)
    {
        return Post::whereDoesntHave('seen_index', function ($q) use ($root) {
            $q->where('user_id', $root->id);
        })->count();
    }
}
