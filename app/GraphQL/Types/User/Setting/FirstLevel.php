<?php
namespace App\GraphQL\Types\User\Setting;

use GraphQL;
use GraphQL\Type\Definition\Type;

use Rebing\GraphQL\Support\Type as GraphQLType;

class FirstLevel extends GraphQLType
{
    protected $attributes = [
        'name' => 'UserSettingFirstLevel',
        'description' => 'A type of input setting'
    ];

    public function fields(): array
    {
        return [
            'field' => ['type' => Type::string(), 'description' => ''],
            'value' => ['type' => Type::string(), 'description' => ''],
            'children' => [ 'type' => Type::listOf(GraphQL::type('user_setting_second_level')), 'description' => '' ],

            '__typename' => ['type' => Type::string(),'description' => 'ignore']
        ];
    }
}


