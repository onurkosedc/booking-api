<?php
namespace App\GraphQL\Types\User\Setting;

use GraphQL;
use GraphQL\Type\Definition\Type;

use Rebing\GraphQL\Support\Type as GraphQLType;

class Group extends GraphQLType
{
    protected $attributes = [
        'name' => 'UserSettingGroup',
        'description' => 'A type of input setting'
    ];

    public function fields(): array
    {
        return [
            'field' => ['type' => Type::string(), 'description' => ''],
            'value' => ['type' => Type::string(), 'description' => ''],
            'children' => [ 'type' => Type::listOf(GraphQL::type('user_setting_first_level')), 'description' => '' ],

            '__typename' => ['type' => Type::string(),'description' => 'ignore']
        ];
    }
}


