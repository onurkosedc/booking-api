<?php
namespace App\GraphQL\Types\User\Setting;

use GraphQL;
use GraphQL\Type\Definition\Type;

use Rebing\GraphQL\Support\Type as GraphQLType;

class ThirdLevel extends GraphQLType
{
    protected $attributes = [
        'name' => 'UserSettingThirdLevel',
        'description' => 'A type of input setting'
    ];

    public function fields(): array
    {
        return [
            'field' => ['type' => Type::string(), 'description' => ''],
            'value' => ['type' => Type::string(), 'description' => ''],

            '__typename' => ['type' => Type::string(),'description' => 'ignore']
        ];
    }
}


