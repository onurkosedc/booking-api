<?php
namespace App\GraphQL\Types\Setting\Input;

use GraphQL;
use GraphQL\Type\Definition\Type;

use Rebing\GraphQL\Support\Type as GraphQLType;

class Setting extends GraphQLType
{
    protected $inputObject = true;

    protected $attributes = [
        'name' => 'SettingInput',
        'description' => 'A type of input setting'
    ];

    public function fields():array
    {
        return [
            'field' => ['type' => Type::string(), 'description' => ''],
            'value' => [ 'type' => Type::string(), 'description' => '' ],

            '__typename' => ['type' => Type::string(),'description' => 'ignore']
        ];
    }
}


