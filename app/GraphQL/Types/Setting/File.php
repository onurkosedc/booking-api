<?php
namespace App\GraphQL\Types\Setting;

use GraphQL;
use GraphQL\Type\Definition\Type;

use Rebing\GraphQL\Support\Type as GraphQLType;

class File extends GraphQLType
{
    protected $attributes = [
        'name' => 'File',
        'description' => 'A type of...'
    ];

    public function fields(): array
    {
        return [
            'id' => ['type' => Type::int(), 'description' => ''],
            'name' => ['type' => Type::string(), 'description' => ''],
            'for' => ['type' => Type::string(), 'description' => ''],

            '__typename' => ['type' => Type::string(),'description' => 'ignore']
        ];
    }
}


