<?php
namespace App\GraphQL\Types\Order;

use GraphQL;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class Order extends GraphQLType
{
    protected $attributes = [
        'name' => 'Order',
        'description' => 'A type',
        'model' => \App\Models\Order::class
    ];

    public function fields(): array
    {
        return [
            'uuid' => [ 'type' => Type::nonNull(Type::id()), 'description' => '' ],
            'order_number' => [ 'type' => Type::string(), 'description' => '' ],
            'postcode' => [ 'type' => Type::string(), 'description' => '' ],
            'email' => [ 'type' => Type::string(), 'description' => '' ],
            'email_alternate' => [ 'type' => Type::string(), 'description' => '' ],

            'user' => [ 'type' => GraphQL::type('user'), 'description' => '' ],
            'deliveries' => [
                'type' => Type::listOf(GraphQL::type('delivery')),
                'description' => ''
            ],
            'has_deliveries' => [
                'type' => Type::boolean(),
                'description' => '',
                'selectable' => false,
                'resolve' => function($root, $args) {
                    return $root->deliveries()->count();
                }
            ],

            'code_id' => ['type' => Type::int(), 'description' => ''],
            'is_fast_track' => [
                'type' => Type::boolean(),
                'description' => '',
                'selectable' => false,
                'resolve' => function ($root) {
                    return $root->code_id > 0;
                }
            ],

            'last_access' => [
                'type' => Type::string(),
                'description' => '',
                'resolve' => function($root, $args) {
                    return $root->last_access->format('d/m/Y H:i');
                }
            ],

            'created_at' => [ 'type' => Type::string(), 'description' => '' ],
            'updated_at' => [ 'type' => Type::string(), 'description' => '' ],
        ];
    }
}
