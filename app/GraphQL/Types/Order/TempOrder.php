<?php
namespace App\GraphQL\Types\Order;

use GraphQL;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class TempOrder extends GraphQLType
{
    protected $attributes = [
        'name' => 'TempOrder',
        'description' => 'A type',
    ];

    public function fields(): array
    {
        return [
            'order_number' => [ 'type' => Type::string(), 'description' => '' ],
            'vendor_name' => [ 'type' => Type::string(), 'description' => '' ],
        ];
    }
}
