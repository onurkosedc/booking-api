<?php
namespace App\GraphQL\Types\Order\Line\Input;

use GraphQL;
use GraphQL\Type\Definition\Type;

use Rebing\GraphQL\Support\Type as GraphQLType;

class Line extends GraphQLType
{
    protected $inputObject = true;

    protected $attributes = [
        'name' => 'LineInput',
        'description' => 'A type of'
    ];

    public function fields(): array
    {
        return [
            'id' => ['type' => Type::int(), 'description' => ''],

            'No' => [ 'type' => Type::string(), 'description' => '' ],
            'Line_No' => [ 'type' => Type::string(), 'description' => '' ],
            'Vendor_Item_No' => [ 'type' => Type::string(), 'description' => '' ],
            'Description' => [ 'type' => Type::string(), 'description' => '' ],
            'Quantity' => [ 'type' => Type::string(), 'description' => '' ],
            'Quantity_Received' => [ 'type' => Type::string(), 'description' => '' ],
            'Qty_to_Receive' => [ 'type' => Type::string(), 'description' => '' ],

            'no' => [ 'type' => Type::string(), 'description' => '' ],
            'qty_to_fulfill' => [ 'type' => Type::float(), 'description' => '' ],

            'created_at' => ['type' => Type::string(), 'description' => ''],
            'updated_at' => ['type' => Type::string(), 'description' => ''],
            'created_by' => ['type' => Type::int(), 'description' => ''],

            '__typename' => ['type' => Type::string(), 'description' => 'ignore'],
        ];
    }
}
