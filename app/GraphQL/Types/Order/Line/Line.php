<?php

namespace App\GraphQL\Types\Order\Line;

use GraphQL;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class Line extends GraphQLType
{
    protected $attributes = [
        'name' => 'Line',
        'description' => 'A type',
    ];

    public function fields(): array
    {
        return [
            /* @note Params derived from orders__lines table */
            'id' => ['type' => Type::int(), 'description' => ''],
            'no' => ['type' => Type::string(), 'description' => ''],
            'line_no' => ['type' => Type::int(), 'description' => ''],
            'qty_to_fulfill' => ['type' => Type::float(), 'description' => ''],

            /* @note Params derived from NAV request */
            'No' => ['type' => Type::string(), 'description' => ''],
            'Line_No' => ['type' => Type::string(), 'description' => ''],
            'Vendor_Item_No' => ['type' => Type::string(), 'description' => ''],
            'Description' => ['type' => Type::string(), 'description' => ''],
            'Quantity' => ['type' => Type::float(), 'description' => ''],
            'Quantity_Received' => ['type' => Type::float(), 'description' => ''],
            'Qty_to_Receive' => ['type' => Type::float(), 'description' => ''],
        ];
    }
}
