<?php
namespace App\GraphQL\Types\Delivery;

use GraphQL;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;
use Illuminate\Support\Str;

class Group extends GraphQLType
{
    protected $attributes = [
        'name' => 'DeliveryGroup',
        'description' => 'A type',
    ];

    public function fields(): array
    {
        return [
            'name' => ['type' => Type::string(), 'description' => ''],
            'total_time' => ['type' => Type::string(), 'description' => ''],
            'deliveries' => ['type' => Type::listOf(GraphQL::type('delivery')), 'description' => ''],
        ];
    }
}
