<?php
namespace App\GraphQL\Types\Delivery;

use GraphQL;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class OrderLine extends GraphQLType
{
    protected $attributes = [
        'name' => 'DeliveryOrderLine',
        'description' => 'A type',
    ];

    public function fields(): array
    {
        return [
            'id' => [ 'type' => Type::int(), 'description' => '' ],
            'no' => [ 'type' => Type::string(), 'description' => '' ],
            'line_no' => [ 'type' => Type::int(), 'description' => '' ],
            'qty_ordered' => [ 'type' => Type::float(), 'description' => '' ],
            'qty_received' => [ 'type' => Type::float(), 'description' => '' ],
            'qty_to_fulfill' => [ 'type' => Type::float(), 'description' => '' ],
            'description' => [ 'type' => Type::string(), 'description' => '' ],
        ];
    }
}
