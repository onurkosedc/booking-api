<?php
namespace App\GraphQL\Types\Delivery\Input;

use GraphQL;
use GraphQL\Type\Definition\Type;

use Rebing\GraphQL\Support\Type as GraphQLType;

class Delivery extends GraphQLType
{
    protected $inputObject = true;

    protected $attributes = [
        'name' => 'DeliveryInput',
        'description' => 'A type of'
    ];

    public function fields(): array
    {
        return [
            'uuid' => ['type' => Type::id(), 'description' => ''],
            'order_uuid' => ['type' => Type::id(), 'description' => ''],
            'date' => ['type' => Type::string(), 'description' => ''],
            'from_time' => ['type' => Type::string(), 'description' => ''],
            'to_time' => ['type' => Type::string(), 'description' => ''],
            'processing_time' => ['type' => Type::int(), 'description' => ''],
            'is_canceled' => ['type' => Type::boolean(), 'description' => ''],
            'confirmation_status' => ['type' => Type::string(), 'description' => ''],
            'reason' => ['type' => Type::string(), 'description' => ''],
            'cancellation_reason' => ['type' => Type::string(), 'description' => ''],
            'status' => ['type' => Type::string(), 'description' => ''],

            'units' => ['type' => Type::listOf(GraphQL::type('unit_input')), 'description' => ''],
            'order_lines' => ['type' => Type::listOf(GraphQL::type('order_line_input')), 'description' => ''],

            'created_at' => ['type' => Type::string(), 'description' => ''],
            'updated_at' => ['type' => Type::string(), 'description' => ''],
            'created_by' => ['type' => Type::int(), 'description' => ''],

            '__typename' => ['type' => Type::string(), 'description' => 'ignore'],
        ];
    }
}
