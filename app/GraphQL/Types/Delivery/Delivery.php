<?php
namespace App\GraphQL\Types\Delivery;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;
use Illuminate\Support\Str;

class Delivery extends GraphQLType
{
    protected $attributes = [
        'name' => 'Delivery',
        'description' => 'A type',
        'model' => \App\Models\Delivery::class
    ];

    public function fields(): array
    {
        return [
            'uuid' => ['type' => Type::id(), 'description' => ''],
            'order_uuid' => ['type' => Type::id(), 'description' => ''],
            'vendor_name' => ['type' => Type::string(), 'description' => ''],
            'from_time' => [
                'type' => Type::string(),
                'description' => '',
                'resolve' => function ($root) {
                    return substr($root->from_time, 0, 5);
                }
            ],
            'to_time' => [
                'type' => Type::string(),
                'description' => '',
                'resolve' => function ($root) {
                    return substr($root->to_time, 0, 5);
                }
            ],
            'processing_time' => ['type' => Type::int(), 'description' => ''],
            'confirmation_status' => ['type' => Type::string(), 'description' => ''],
            'confirmation_changed_at' => [
                'type' => Type::string(),
                'description' => '',
                'resolve' => function ($root) {
                    return is_null($root->confirmation_changed_at) ? '' : $root->confirmation_changed_at->format('d/m/Y H:i');
                }
            ],
            'is_canceled' => ['type' => Type::boolean(), 'description' => ''],
            'is_cancellation_notified' => ['type' => Type::boolean(), 'description' => ''],
            'canceled_at' => [
                'type' => Type::string(),
                'description' => '',
                'resolve' => function ($root) {
                    return is_null($root->canceled_at) ? '' : $root->canceled_at->format('d/m/Y H:i');
                }
            ],
            'delivery_date' => [
                'type' => Type::string(),
                'description' => '',
                'resolve' => function ($root) {
                    return is_null($root->delivery_date) ? '' : $root->delivery_date->format('d/m/Y');
                }
            ],

            'status' => ['type' => Type::string(), 'description' => ''],
            'status_pretty' => [
                'type' => Type::string(),
                'description' => '',
                'selectable' => false,
                'resolve' => function ($root) {
                    return Str::of(str_replace('_', ' ', $root->status))->title();
                }
            ],

            'units' => [
                'type' => Type::listOf(GraphQL::type('delivery_unit')),
                'description' => '',
                'args' => [
                    'units' => ['name' => 'units', 'type' => Type::listOf(GraphQL::type('unit_input'))],
                ],
                'query' => function(array $args, $query) {
                    if (isset($args['units'])) {
                        if (is_array($args['units'])) {
                            return $query->whereIn('unit_id', collect($args['units'])->map(function ($unit) { return $unit['id']; }));
                        }
                    }

                    return $query;
                }
            ],

            'order' => ['type' => GraphQL::type('order'), 'description' => ''],
            'order_lines' => ['type' => Type::listOf(GraphQL::type('delivery_order_line')), 'description' => ''],

            'code_id' => ['type' => Type::int(), 'description' => ''],
            'is_fast_track' => [
                'type' => Type::boolean(),
                'description' => '',
                'selectable' => false,
                'resolve' => function ($root) {
                    return $root->code_id > 0;
                }
            ],

            'created_at' =>  [
                'type' => Type::string(),
                'description' => '',
                'resolve' => function ($root) {
                    return is_null($root->created_at) ? '' : $root->created_at->format('d/m/Y H:i');
                }
            ],
            'updated_at' =>  [
                'type' => Type::string(),
                'description' => '',
                'resolve' => function ($root) {
                    return is_null($root->updated_at) ? '' : $root->updated_at->format('d/m/Y H:i');
                }
            ],
            'created_by' => ['type' => Type::int(), 'description' => ''],
        ];
    }
}
