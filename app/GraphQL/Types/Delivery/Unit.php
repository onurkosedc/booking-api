<?php
namespace App\GraphQL\Types\Delivery;

use GraphQL;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class Unit extends GraphQLType
{
    protected $attributes = [
        'name' => 'DeliveryUnit',
        'description' => 'A type',
        'model' => \App\Models\DeliveryUnit::class
    ];

    public function fields(): array
    {
        return [
            'id' => ['type' => Type::int(), 'description' => ''],
            'delivery_uuid' => ['type' => Type::id(), 'description' => ''],
            'unit_id' => ['type' => Type::int(), 'description' => ''],
            'quantity' => ['type' => Type::int(), 'description' => ''],
            'processing_time' => ['type' => Type::int(), 'description' => ''],
        ];
    }
}
