<?php

namespace App\GraphQL\Concerns;

use Illuminate\Http\Request;

trait GetOrigin
{
    /** @var string */
    public $domain;

    public function getOriginHost(Request $request)
    {
        $requestHost = parse_url($request->headers->get('origin'),  PHP_URL_HOST);

        switch ($requestHost) {
            case 'deliveries.totalfulfilment.co.uk':
                $this->domain = 'TF';
                break;
            case 'deliveries.yorkshiretrading.com':
                $this->domain = 'YTC';
                break;
            default:
                $this->domain = 'LCL';
        }
    }
}
