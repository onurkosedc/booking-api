<?php

namespace App\GraphQL\Concerns;

use ODataQueryBuilder\ODataQueryBuilder;

trait NavFeedTrait
{
    /**
     * @return ODataQueryBuilder
     */
    private function getBuilder(): ODataQueryBuilder
    {
        return new ODataQueryBuilder('');
    }

    /**
     * @param $url
     * @param null $status
     * @param int $wait
     * @return mixed
     */
    private function http_response($url, $status = null, $wait = 3)
    {
        $odataURL = config('custom.odata_url');

        $url = $odataURL . preg_replace('/\?/', '?$format=json&', $url, 1);

        $time = microtime(true);
        $expire = $time + $wait;

        $username = "YTCNET\webservice";
        $password = 'RUbttjt8WcPttUneT8YDTuU';

        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_GSSAPI_DELEGATION, CURLGSSAPI_DELEGATION_FLAG);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_NTLM | CURLAUTH_GSSNEGOTIATE);
        curl_setopt($ch, CURLOPT_USERPWD, $username.":".$password);
        curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, 10000);

        $result = curl_exec($ch);

        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $curl_errno = curl_errno($ch);
        $curl_error = curl_error($ch);

        if ($curl_errno > 0) {
            $json = ['timeout' => true];
        } else {
            $json = json_decode($result, true);
        }

        curl_close($ch);

        return $json;
    }
}
