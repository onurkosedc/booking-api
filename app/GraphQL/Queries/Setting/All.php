<?php
namespace App\GraphQL\Queries\Setting;

use App\GraphQL\Concerns\GetOrigin;

use App\Models\Company;
use Closure;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

use Illuminate\Http\Request;

class All extends Query
{
    Use GetOrigin;

    protected $attributes = [
        'name' => 'AllSettings', 'description' => 'A query'
    ];

    protected $user;

    public function __construct(Request $request)
    {
        $this->user = $request->user();

        $this->getOriginHost($request);
    }

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('setting'));
    }

    public function args(): array
    {
        return [];
    }

    public function resolve($root, $args, $context, ResolveInfo $info, SelectFields $fields)
    {
        $company = Company::firstorCreate([
            'name' => $this->domain
        ]);

        $settings = $company->settings()->all();

        $results = [];

        foreach ($settings as $field => $value) {
            $results[] = [
                'field' => $field,
                'value' => $value
            ];
        }

        return $results;
    }
}
