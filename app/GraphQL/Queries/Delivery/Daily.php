<?php
namespace App\GraphQL\Queries\Delivery;

use App\Models\Delivery;

use Auth;
use Closure;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

use Illuminate\Http\Request;

class Daily extends Query
{
    protected $attributes = [
        'name' => 'Daily', 'description' => 'A query'
    ];

    protected $user;

    public function __construct(Request $request)
    {
        $this->user = $request->user();
    }

    public function authorize($root, array $args, $ctx, ResolveInfo $resolveInfo = null, Closure $getSelectFields = null): bool
    {
        return true; //return Auth::check() ? $this->user->is_admin : false;
    }

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('delivery'));
    }

    public function args(): array
    {
        return [
            'date' => ['name' => 'date', 'type' => Type::nonNull(Type::string())],
            'units' => ['name' => 'units', 'type' => Type::listOf(GraphQL::type('unit_input'))],
        ];
    }

    protected function rules(array $args = []): array
    {
        return [
            'date' => ['date'],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $fields = $getSelectFields();
        $select = $fields->getSelect();
        $with = $fields->getRelations();

        return Delivery::with($with)
            ->whereHas('units', function ($query) use ($args) {
                $query->whereIn('unit_id', collect($args['units'])->map(function ($unit) { return $unit['id']; }));
            })
            ->whereNotIn('status', ['cancelled_by_vendor', 'cancelled_by_admin', 'rejected_reservation'])
            ->whereDate('delivery_date', $args['date'])
            ->orderBy('from_time', 'asc')
            ->get();
    }
}
