<?php
namespace App\GraphQL\Queries\Delivery;

use App\Models\Delivery;
use Auth;
use Closure;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use ODataQueryBuilder\ODataQueryBuilder;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

use Illuminate\Http\Request;

class Get extends Query
{
    protected $attributes = [
        'name' => 'Get', 'description' => 'A query'
    ];

    protected $user;

    public function __construct(Request $request)
    {
        $this->user = $request->user();
    }

    public function authorize($root, array $args, $ctx, ResolveInfo $resolveInfo = null, Closure $getSelectFields = null): bool
    {
        return true; //return Auth::check() ? $this->user->is_admin : false;
    }

    public function type(): Type
    {
        return GraphQL::type('delivery');
    }

    public function args(): array
    {
        return [
            'uuid' => ['name' => 'uuid', 'type' => Type::nonNull(Type::id())],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $fields = $getSelectFields();
        $select = $fields->getSelect();
        $with = $fields->getRelations();

        $deliveryModel = Delivery::select($select)->with($with)->find($args['uuid']);

        if (is_null($deliveryModel)) {
            throw new GraphQL\Error\Error('delivery_not_found');
        }

        return $deliveryModel;
    }
}
