<?php
namespace App\GraphQL\Queries\Delivery;

use App\Models\Delivery;
use App\Models\Order;
use App\Models\Unit;

use Auth;
use Carbon\CarbonInterval;
use Closure;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

use Illuminate\Http\Request;

class Grouped extends Query
{
    protected $attributes = [
        'name' => 'Grouped', 'description' => 'A query'
    ];

    protected $user;

    public function __construct(Request $request)
    {
        $this->user = $request->user();
    }

    public function authorize($root, array $args, $ctx, ResolveInfo $resolveInfo = null, Closure $getSelectFields = null): bool
    {
        return Auth::check() ? $this->user->is_admin : false;
    }

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('delivery_group'));
    }

    public function args(): array
    {
        return [];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $fields = $getSelectFields();

        $todaysDate = new \DateTime('now');
        $tomorrowsDate = new \DateTime('+1 day');
        $thisWeekEndDate = new \DateTime('+6 days this week monday');
        $nextWeekStartDate = new \DateTime('next week monday');
        $nextWeekEndDate = new \DateTime('+6 days next week monday');

        $today = Delivery::with($fields->getRelations())
            ->whereDate('delivery_date', $todaysDate->format('Y-m-d'))
            ->orderBy('delivery_date', 'asc')
            ->get();

        $tomorrow = Delivery::with($fields->getRelations())
            ->whereDate('delivery_date', $tomorrowsDate->format('Y-m-d'))
            ->orderBy('delivery_date', 'asc')
            ->get();

        $thisWeek = Delivery::with($fields->getRelations())
            ->whereDate('delivery_date', '>=', $todaysDate->format('Y-m-d'))
            ->whereDate('delivery_date', '<=', $thisWeekEndDate->format('Y-m-d'))
            ->orderBy('delivery_date', 'asc')
            ->get();

        $nextWeek = Delivery::with($fields->getRelations())
            ->whereDate('delivery_date', '>=', $nextWeekStartDate->format('Y-m-d'))
            ->whereDate('delivery_date', '<=', $nextWeekEndDate->format('Y-m-d'))
            ->orderBy('delivery_date', 'asc')
            ->get();

        return [
            [
                'name' => 'today',
                'total_time' => $this->getTotalProcessingTime($today),
                'deliveries' => $today,
            ],
            [
                'name' => 'tomorrow',
                'total_time' => $this->getTotalProcessingTime($tomorrow),
                'deliveries' => $tomorrow,
            ],
            [
                'name' => 'thisWeek',
                'total_time' => $this->getTotalProcessingTime($thisWeek),
                'deliveries' => $thisWeek
            ],
            [
                'name' => 'nextWeek',
                'total_time' => $this->getTotalProcessingTime($nextWeek),
                'deliveries' => $nextWeek
            ]
        ];
    }

    /**
     * @param $collection
     * @return string
     */
    private function getTotalProcessingTime($collection): string
    {
        /** @var Delivery $item */
        $interval = $collection->reduce(function ($carry, $item) {
            return $carry + $item->processing_time;
        });

        if ($interval > 0) {
            return CarbonInterval::minutes($interval)->cascade()->forHumans(['join' => true]);
        }

        return '';
    }
}
