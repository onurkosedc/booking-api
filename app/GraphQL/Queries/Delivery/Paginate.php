<?php
namespace App\GraphQL\Queries\Delivery;

use App\Models\Delivery;
use App\Models\Order;
use App\Models\Unit;

use Closure;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

use Illuminate\Http\Request;

class Paginate extends Query
{
    protected $attributes = [
        'name' => 'Paginate', 'description' => 'A query'
    ];

    protected $user;
    protected $filters;

    public function __construct(Request $request)
    {
        $this->user = $request->user();

        $this->filters = [
            'order_number',
            'delivery_date_start',
            'delivery_date_end',
            'status',
            'multiple_status',
            'confirmation_status',
            'multiple_confirmation_status',
            'unit_qty',
            'units',
        ];
    }

    public function authorize($root, array $args, $ctx, ResolveInfo $resolveInfo = null, Closure $getSelectFields = null): bool
    {
        return Auth::check() ? $this->user->is_admin : false;
    }

    public function type(): Type
    {
        return GraphQL::paginate('delivery');
    }

    public function args(): array
    {
        return [
            'filter' => ['name' => 'filter', 'type' => Type::listOf(GraphQL::type('generic_filter_input'))],

            'sortBy' => ['name' => 'sortBy', 'type' => Type::string()],
            'descending' => ['name' => 'descending', 'type' => Type::boolean()],
            'page' => ['name' => 'page', 'type' => Type::int()],
            'rowsPerPage' => ['name' => 'rowsPerPage', 'type' => Type::int()]
        ];
    }

    protected function rules(array $args = []): array
    {
        return [
            'date' => ['date'],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $fields = $getSelectFields();
        $select = $fields->getSelect();
        $with = $fields->getRelations();

        foreach ($args['filter'] as $filter) {
            if (array_search($filter['field'], $this->filters) !== false) {
                continue;
            } else {
                return null;
            }
        }

        isset($args['sortBy']) || $args['sortBy'] = 'uuid';

        $args['order'] = 'asc';

        if (isset($args['descending'])) {
            $args['order'] = $args['descending'] ? 'desc' : 'asc';
        }

        $query = Delivery::select($select)->with($with)->orderBy($args['sortBy'], $args['order']);

        foreach ($args['filter'] as $filter) {
            $value = data_get($filter, 'value', null);
            $values = data_get($filter, 'values', null);

            throw_if(is_null($value) && is_null($values), new GraphQL\Error\Error('needs_value'));

            switch ($filter['field']) {
                case 'order_number':
                    $query->whereHas('order', function ($query) use ($filter, $value) {
                        return $query->where($filter['field'], 'LIKE', "%{$value}%");
                    });
                    break;

                case 'status':
                case 'confirmation_status':
                    $query->where($filter['field'], $value);
                    break;

                case 'delivery_date_start':
                    $endDate = collect($args['filter'])->where('field', 'delivery_date_end')->first();

                    $hasEndDate = is_array($endDate) ? !empty($endDate['value']) : false;

                    if ($hasEndDate) {
                        $query->whereDate('delivery_date', '>=', $value);
                    } else {
                        $query->where('delivery_date', $value);
                    }
                    break;

                case 'delivery_date_end':
                    $query->whereDate('delivery_date', '<=', $value);
                    break;

                case 'multiple_status':
                    $query->whereIn('status', $values);
                    break;

                case 'multiple_confirmation_status':
                    $query->whereIn('confirmation_status', $values);
                    break;

                case 'units':
                    $query->whereHas('units', function ($query) use ($values) {
                        return $query->whereIn('unit_id', $values);
                    });
                    break;

                case 'unit_qty':
                    $query->whereHas('units', function ($query) use ($value) {
                        return $query->where('quantity', $value);
                    });
                    break;
            }
        }

        $args['rowsPerPage'] = $args['rowsPerPage'] ?: $query->count();

        return $query->paginate($args['rowsPerPage'], ['*'], 'page', $args['page']);
    }
}
