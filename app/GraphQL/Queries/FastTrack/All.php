<?php
namespace App\GraphQL\Queries\FastTrack;

use App\Models\FastTrack;

use Auth;
use Closure;
use GraphQL;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

use Illuminate\Http\Request;

class All extends Query
{
    protected $attributes = [
        'name' => 'AllFastTrack', 'description' => 'A query'
    ];

    protected $user;

    public function __construct(Request $request)
    {
        $this->user = $request->user();
    }

    public function authorize($root, array $args, $ctx, ResolveInfo $resolveInfo = null, Closure $getSelectFields = null): bool
    {
        return Auth::check() ? $this->user->is_admin : false;
    }

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('fast_track'));
    }

    public function args(): array
    {
        return [
            'email' => ['name' => 'email', 'type' => Type::string()],
            'code' => ['name' => 'code', 'type' => Type::boolean()],

            'sortBy' => ['name' => 'sortBy', 'type' => Type::string()],
            'descending' => ['name' => 'descending', 'type' => Type::boolean()]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info, SelectFields $fields)
    {
        isset($args['sortBy']) || $args['sortBy'] = 'id';

        $args['order'] = 'desc';

        if (isset($args['descending'])) {
            $args['order'] = $args['descending'] ? 'desc' : 'asc';
        }

        $codes = FastTrack::orderBy($args['sortBy'], $args['order'])->get();

        return $codes;
    }
}
