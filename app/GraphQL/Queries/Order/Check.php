<?php
namespace App\GraphQL\Queries\Order;

use App\Models\Delivery;
use App\Models\FastTrack;
use App\Models\Order;
use App\Models\Unit;

use Auth;
use Closure;
use GraphQL;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use ODataQueryBuilder\ODataQueryBuilder;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

use Illuminate\Http\Request;

class Check extends Query
{
    protected $attributes = [
        'name' => 'Check', 'description' => 'A query'
    ];

    protected $user;

    public function __construct(Request $request)
    {
        $this->user = $request->user();
    }

    public function authorize($root, array $args, $ctx, ResolveInfo $resolveInfo = null, Closure $getSelectFields = null): bool
    {
        return true; //return Auth::check() ? $this->user->is_admin : false;
    }

    public function type(): Type
    {
        return GraphQL::type('order');
    }

    public function args(): array
    {
        return [
            'orderNumber' => ['name' => 'orderNumber', 'type' => Type::nonNull(Type::string())],
            'postcode' => ['name' => 'postcode', 'type' => Type::nonNull(Type::string())],
            'email' => ['name' => 'email', 'type' => Type::nonNull(Type::string())],
            'emailAlternate' => ['name' => 'emailAlternate', 'type' => Type::string()],
            'useCode' => ['name' => 'useCode', 'type' => Type::boolean()],
            'code' => ['name' => 'code', 'type' => Type::string()],
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $info, SelectFields $fields)
    {
        $fastTrackCodeId = null;
        $useCode = data_get($args, 'useCode', false);

        if ($useCode === true) {
            /** @var FastTrack $code */
            $code = FastTrack::where('code', $args['code'])->first();

            if (is_null($code)) {
                throw new GraphQL\Error\Error('code_not_found');
            } elseif ($code->delivery()->whereHas('order', function ($query) use ($args) {
                $query->where('order_number', '!=', $args['orderNumber']);
            })->exists()) {
                throw new GraphQL\Error\Error('code_not_found');
            } else {
                $fastTrackCodeId = $code->id;
            }
        }

        $orderHeadersQuery = $this->getBuilder()->from('Purchase_Order_Headers')->filterWhere('No')->equals($args['orderNumber'])->buildQuery();

        $orderHeaders = $this->http_response($orderHeadersQuery);

        if (isset($orderHeaders['timeout'])) {
            throw new GraphQL\Error\Error('feed_timeout');
        } else if (!isset($orderHeaders['value'])) {
            throw new GraphQL\Error\Error('missing_value');
        }

        if (count($orderHeaders['value'])) {
            $order = $orderHeaders['value'][0];

            $clientPostcodeFixed = preg_replace("/\s+/", '', $args['postcode']);
            $orderPostcodeFixed = preg_replace("/\s+/", '', $order['Buy_from_Post_Code']);

            if ($clientPostcodeFixed == $orderPostcodeFixed || strtolower($clientPostcodeFixed) == strtolower($orderPostcodeFixed)) {
                $orderModel = Order::updateOrCreate([
                    'order_number' => $order['No'],
                    'postcode' => $order['Buy_from_Post_Code'],
                ], [
                    'vendor_name' => $order['Buy_from_Vendor_Name'],
                    'email' => $args['email'],
                    'email_alternate' => $args['emailAlternate'],
                    'code_id' => $fastTrackCodeId,
                    'last_access' => new \DateTime('now')
                ]);

                Delivery::where('order_uuid', $orderModel->uuid)->update([
                    'vendor_name' => $order['Buy_from_Vendor_Name'],
                ]);

                return $orderModel;
            }
        }

        throw new GraphQL\Error\Error('order_not_found');
    }

    /**
     * @return ODataQueryBuilder
     */
    private function getBuilder(): ODataQueryBuilder
    {
        return new ODataQueryBuilder('');
    }

    /**
     * @param $url
     * @param null $status
     * @param int $wait
     * @return mixed
     */
    private function http_response($url, $status = null, $wait = 3)
    {
        $odataURL = config('custom.odata_url');

        $url = $odataURL . preg_replace('/\?/', '?$format=json&', $url, 1);

        $time = microtime(true);
        $expire = $time + $wait;

        $username = "YTCNET\webservice";
        $password = 'RUbttjt8WcPttUneT8YDTuU';

        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_GSSAPI_DELEGATION, CURLGSSAPI_DELEGATION_FLAG);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_NTLM | CURLAUTH_GSSNEGOTIATE);
        curl_setopt($ch, CURLOPT_USERPWD, $username.":".$password);
        curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, 10000);

        $result = curl_exec($ch);

        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $curl_errno = curl_errno($ch);
        $curl_error = curl_error($ch);

        if ($curl_errno > 0) {
            $json = ['timeout' => true];
        } else {
            $json = json_decode($result, true);
        }

        curl_close($ch);

        return $json;
    }
}
