<?php
namespace App\GraphQL\Queries\Order;

use App\Models\Order;
use App\Models\Unit;

use Auth;
use Closure;
use GraphQL;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use ODataQueryBuilder\ODataQueryBuilder;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

use Illuminate\Http\Request;

class Get extends Query
{
    protected $attributes = [
        'name' => 'Get', 'description' => 'A query'
    ];

    protected $user;

    public function __construct(Request $request)
    {
        $this->user = $request->user();
    }

    public function authorize($root, array $args, $ctx, ResolveInfo $resolveInfo = null, Closure $getSelectFields = null): bool
    {
        return true; //return Auth::check() ? $this->user->is_admin : false;
    }

    public function type(): Type
    {
        return GraphQL::type('order');
    }

    public function args(): array
    {
        return [
            'uuid' => ['name' => 'uuid', 'type' => Type::nonNull(Type::id())],
            'postcode' => ['name' => 'postcode', 'type' => Type::string()],
            'refetch' => ['name' => 'refetch', 'type' => Type::string()],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $fields = $getSelectFields();
        $select = $fields->getSelect();
        $with = $fields->getRelations();

        $orderModel = Order::select($select)->with($with)->find($args['uuid']);

        if (is_null($orderModel)) {
            throw new GraphQL\Error\Error('order_not_found');
        } elseif ($orderModel->updated_at < new \DateTime('-5 minutes')) {
            if (isset($args['postcode'])) {
                if ($args['postcode'] != $orderModel->postcode) {
                    throw new GraphQL\Error\Error('not_confirmed');
                }
            } else {
                throw new GraphQL\Error\Error('needs_confirmation');
            }
        }

        $orderModel->update([
            'last_access' =>  new \DateTime('now')
        ]);

        return $orderModel;
    }
}
