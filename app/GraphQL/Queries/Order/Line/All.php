<?php
namespace App\GraphQL\Queries\Order\Line;

use App\Models\Order;
use App\Models\Unit;

use Auth;
use Closure;
use GraphQL;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use ODataQueryBuilder\ODataQueryBuilder;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

use Illuminate\Http\Request;

class All extends Query
{
    protected $attributes = [
        'name' => 'All', 'description' => 'A query'
    ];

    protected $user;

    public function __construct(Request $request)
    {
        $this->user = $request->user();
    }

    public function authorize($root, array $args, $ctx, ResolveInfo $resolveInfo = null, Closure $getSelectFields = null): bool
    {
        return true; //return Auth::check() ? $this->user->is_admin : false;
    }

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('order_line'));
    }

    public function args(): array
    {
        return [
            'uuid' => ['name' => 'uuid', 'type' => Type::string()],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $orderModel = Order::find($args['uuid']);

        $orderLinesQuery = $this->getBuilder()->from('Purchase_Order_Lines')->filterWhere('Document_No')->equals($orderModel->order_number)->buildQuery();

        $orderLines = $this->http_response($orderLinesQuery);

        if (isset($orderLines['timeout'])) {
            throw new GraphQL\Error\Error('feed_timeout');
        } else if (!isset($orderLines['value'])) {
            throw new GraphQL\Error\Error('missing_value');
        }

        if (count($orderLines['value'])) {
            return $orderLines['value'];
        } else {
            throw new GraphQL\Error\Error('order_lines_not_found');
        }
    }

    /**
     * @return ODataQueryBuilder
     */
    private function getBuilder(): ODataQueryBuilder
    {
        return new ODataQueryBuilder('');
    }

    /**
     * @param $url
     * @param null $status
     * @param int $wait
     * @return mixed
     */
    private function http_response($url, $status = null, $wait = 3)
    {
        $odataURL = config('custom.odata_url');

        $url = $odataURL . preg_replace('/\?/', '?$format=json&', $url, 1);

        $time = microtime(true);
        $expire = $time + $wait;

        $username = "YTCNET\webservice";
        $password = 'RUbttjt8WcPttUneT8YDTuU';

        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_GSSAPI_DELEGATION, CURLGSSAPI_DELEGATION_FLAG);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_NTLM | CURLAUTH_GSSNEGOTIATE);
        curl_setopt($ch, CURLOPT_USERPWD, $username.":".$password);
        curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, 10000);

        $result = curl_exec($ch);

        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $curl_errno = curl_errno($ch);
        $curl_error = curl_error($ch);

        if ($curl_errno > 0) {
            $json = ['timeout' => true];
        } else {
            $json = json_decode($result, true);
        }

        curl_close($ch);

        return $json;
    }
}
