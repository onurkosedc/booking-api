<?php

namespace App\GraphQL\Queries\_Order;

use App\GraphQL\Concerns\NavFeedTrait;
use Closure;
use GraphQL;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Illuminate\Support\Facades\Auth;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

use Illuminate\Http\Request;

class Check extends Query
{
    use NavFeedTrait;

    protected $attributes = [
        'name' => 'Check', 'description' => 'A query'
    ];

    public function __construct()
    {
    }

    public function type(): Type
    {
        return GraphQL::type('temp_order');
    }

    public function args(): array
    {
        return [
            'documentNumber' => ['name' => 'documentNumber', 'type' => Type::nonNull(Type::string())],
            'postCode' => ['name' => 'postCode', 'type' => Type::nonNull(Type::string())],
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $info, SelectFields $fields)
    {
        $orderHeadersQuery = $this->getBuilder()->from('Purchase_Order_Headers')->filterWhere('No')->equals($args['documentNumber'])->buildQuery();

        $orderHeaders = $this->http_response($orderHeadersQuery);

        if (isset($orderHeaders['timeout'])) {
            throw new GraphQL\Error\Error('feed_timeout');
        } else if (!isset($orderHeaders['value'])) {
            throw new GraphQL\Error\Error('missing_value');
        }

        if (count($orderHeaders['value'])) {
            $order = $orderHeaders['value'][0];

            $clientPostcodeFixed = preg_replace("/\s+/", '', $args['postCode']);
            $orderPostcodeFixed = preg_replace("/\s+/", '', $order['Buy_from_Post_Code']);

            if ($clientPostcodeFixed == $orderPostcodeFixed || strtolower($clientPostcodeFixed) == strtolower($orderPostcodeFixed)) {
                return [
                    'order_number' => $order['No'],
                    'vendor_name' => $order['Buy_from_Vendor_Name']
                ];
            }
        }

        throw new GraphQL\Error\Error('order_not_found');
    }

}
