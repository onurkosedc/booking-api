<?php
namespace App\GraphQL\Queries\Unit\Schedule;

use App\Models\Schedule;

use Auth;
use Closure;
use GraphQL;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

use Illuminate\Http\Request;

class All extends Query
{
    protected $attributes = [
        'name' => 'AllSchedule', 'description' => 'A query'
    ];

    protected $user;

    public function __construct(Request $request)
    {
        $this->user = $request->user();
    }

    public function authorize($root, array $args, $ctx, ResolveInfo $resolveInfo = null, Closure $getSelectFields = null): bool
    {
        return true; // Auth::check() ? $this->user->is_admin : false;
    }

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('unit_schedule'));
    }

    public function args(): array
    {
        return [
            'unitId' => ['name' => 'unitId', 'type' => Type::nonNull(Type::int())],

            'sortBy' => ['name' => 'sortBy', 'type' => Type::string()],
            'descending' => ['name' => 'descending', 'type' => Type::boolean()]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info, SelectFields $fields)
    {
        isset($args['sortBy']) || $args['sortBy'] = 'id';

        $args['order'] = 'asc';

        if (isset($args['descending'])) {
            $args['order'] = $args['descending'] ? 'desc' : 'asc';
        }

        $schedule = Schedule::where('unit_id', $args['unitId'])->orderBy($args['sortBy'], $args['order'])->get();

        return $schedule;
    }
}
