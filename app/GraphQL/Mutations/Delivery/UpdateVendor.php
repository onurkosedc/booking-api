<?php
namespace App\GraphQL\Mutations\Delivery;

use App\Mail\CancelDelivery;
use App\Mail\AcceptedReservation;
use App\Models\Delivery;

use Mail;
use Auth;
use Closure;
use GraphQL;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

use Illuminate\Http\Request;

class UpdateVendor extends Mutation
{
    protected $attributes = ['name' => 'UpdateDeliveryByVendor'];

    protected $user;

    public function __construct(Request $request)
    {
        $this->user = $request->user();
    }

    public function authorize($root, array $args, $ctx, ResolveInfo $resolveInfo = null, Closure $getSelectFields = null): bool
    {
        return true; //Auth::check() ? $this->user->is_admin : false;
    }

    public function type(): Type
    {
        return GraphQL::type('delivery');
    }

    public function args(): array
    {
        return [
            'delivery' => ['name' => 'delivery', 'type' => Type::nonNull(GraphQL::type('delivery_input'))],
        ];
    }
    public function resolve($root, $args)
    {
        $delivery = $args['delivery'];

        $deliveryModel = Delivery::with('order')->find($delivery['uuid']);

        if (is_null($deliveryModel)) {
            throw new GraphQL\Error\Error('delivery_not_found');
        }

        if (isset($delivery['is_canceled'])) {
            if ($delivery['is_canceled']) {
                $updatedData = [
                    'is_canceled' => $delivery['is_canceled'],
                    'canceled_at' => new \DateTime('now'),
                    'canceled_by' => null,
                    'cancellation_reason' => $delivery['cancellation_reason'],
                    'status' => 'cancelled_by_vendor'
                ];

                $deliveryModel->update($updatedData);

                $mail = Mail::to($deliveryModel->order->email);

                if (!empty($deliveryModel->order->email_alternate)) {
                    $mail->cc($deliveryModel->order->email_alternate);
                }

                $mail->send(new CancelDelivery($deliveryModel, $delivery['cancellation_reason']));

                $deliveryModel->update([
                    'is_cancellation_notified' => true
                ]);
            }
        }

        return $deliveryModel;
    }
}
