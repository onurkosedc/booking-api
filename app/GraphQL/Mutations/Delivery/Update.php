<?php
namespace App\GraphQL\Mutations\Delivery;

use App\Mail\CancelDelivery;
use App\Mail\AcceptedReservation;
use App\Mail\DeliveryConfirmationCancellation;
use App\Mail\RejectedReservation;
use App\Mail\RevokeCanceledDelivery;
use App\Models\Delivery;

use Mail;
use Auth;
use Closure;
use GraphQL;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

use Illuminate\Http\Request;

class Update extends Mutation
{
    protected $attributes = ['name' => 'UpdateDelivery'];

    protected $user;

    public function __construct(Request $request)
    {
        $this->user = $request->user();
    }

    public function authorize($root, array $args, $ctx, ResolveInfo $resolveInfo = null, Closure $getSelectFields = null): bool
    {
        return Auth::check() ? $this->user->is_admin : false;
    }

    public function type(): Type
    {
        return GraphQL::type('delivery');
    }

    public function args(): array
    {
        return [
            'delivery' => ['name' => 'delivery', 'type' => Type::nonNull(GraphQL::type('delivery_input'))],
        ];
    }
    public function resolve($root, $args)
    {
        $delivery = $args['delivery'];

        $deliveryModel = Delivery::with('order', 'units.unit')->find($delivery['uuid']);

        if (is_null($deliveryModel)) {
            throw new GraphQL\Error\Error('delivery_not_found');
        }

        if (isset($delivery['status'])) {
            $deliveryModel->update([
                'status' => $delivery['status']
            ]);
        }

        if (isset($delivery['confirmation_status'])) {
            switch ($delivery['confirmation_status']) {
                case 'accepted':
                    $updatedData = [
                        'confirmation_status' => $delivery['confirmation_status'],
                        'confirmation_changed_at' => now(),
                        'confirmed_by' => $this->user->id,
                        'status' => 'pending_delivery'
                    ];
                    break;
                case 'rejected':
                    $updatedData = [
                        'confirmation_status' => $delivery['confirmation_status'],
                        'confirmation_changed_at' => now(),
                        'confirmed_by' => $this->user->id,
                        'is_confirmation_notified' => null,
                        'status' => 'rejected_reservation'
                    ];
                    break;
                default:
                    $updatedData = [
                        'confirmation_status' => $delivery['confirmation_status'],
                        'confirmation_changed_at' => now(),
                        'confirmed_by' => $this->user->id,
                        'is_confirmation_notified' => null,
                        'status' => 'pending_confirmation'
                    ];
                    break;
            }

            $deliveryModel->update($updatedData);

            if (in_array($delivery['confirmation_status'], ['accepted', 'rejected'])) {
                $mail = Mail::to($deliveryModel->order->email);

                if (!empty($deliveryModel->order->email_alternate)) {
                    $mail->cc($deliveryModel->order->email_alternate);
                }

                if ($delivery['confirmation_status'] === 'accepted') {
                    $mail->send(new AcceptedReservation($deliveryModel));
                } elseif ($delivery['confirmation_status'] === 'rejected') {
                    $mail->send(new RejectedReservation($deliveryModel, $delivery['reason']));
                }

                $deliveryModel->update([
                    'is_confirmation_notified' => true
                ]);
            }
        }

        if (isset($delivery['is_canceled'])) {
            if ($delivery['is_canceled']) {
                $updatedData = [
                    'is_canceled' => true,
                    'canceled_at' => now(),
                    'canceled_by' => $this->user->id,
                    'status' => 'cancelled_by_admin'
                ];
            } else {
                switch ($deliveryModel->confirmation_status) {
                    case 'accepted':
                        $newStatus = 'pending_delivery';
                        break;
                    case 'rejected':
                        $newStatus = 'rejected_reservation';
                        break;
                    default:
                        $newStatus = 'pending_confirmation';
                        break;
                }

                $updatedData = [
                    'is_canceled' => false,
                    'canceled_at' => null,
                    'canceled_by' => null,
                    'is_cancellation_notified' => null,
                    'status' => $newStatus
                ];
            }

            $deliveryModel->update($updatedData);

            $mail = Mail::to($deliveryModel->order->email);

            if (!empty($deliveryModel->order->email_alternate)) {
                $mail->cc($deliveryModel->order->email_alternate);
            }

            if ($delivery['is_canceled']) {
                $mail->send(new CancelDelivery($deliveryModel, $delivery['reason']));
            } else {
                $mail->send(new RevokeCanceledDelivery($deliveryModel));
            }

            $deliveryModel->update([
                'is_cancellation_notified' => true
            ]);
        }

        return $deliveryModel;
    }
}
