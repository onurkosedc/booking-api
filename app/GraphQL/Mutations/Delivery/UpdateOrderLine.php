<?php
namespace App\GraphQL\Mutations\Delivery;

use App\Models\Delivery;
use App\Models\DeliveryUnit;
use App\Models\OrderLine;
use App\Models\Unit;

use Auth;
use Closure;
use GraphQL;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

use Illuminate\Http\Request;

class UpdateOrderLine extends Mutation
{
    protected $attributes = ['name' => 'UpdateDeliveryOrderLine'];

    protected $user;

    public function __construct(Request $request)
    {
        $this->user = $request->user();
    }

    public function authorize($root, array $args, $ctx, ResolveInfo $resolveInfo = null, Closure $getSelectFields = null): bool
    {
        return true; // Auth::check() ? $this->user->is_admin : false;
    }

    public function type(): Type
    {
        return GraphQL::type('delivery');
    }

    public function args(): array
    {
        return [
            'delivery' => ['name' => 'delivery', 'type' => Type::nonNull(GraphQL::type('delivery_input'))],
        ];
    }
    public function resolve($root, $args)
    {
        $delivery = $args['delivery'];

        $deliveryModel = Delivery::find($delivery['uuid']);

        if (is_null($deliveryModel)) {
            throw new GraphQL\Error\Error('delivery_not_found');
        }

        $lineIds = [];

        foreach ($delivery['order_lines'] as $orderLine) {
            $controlData = [
                'order_uuid' => $deliveryModel->order_uuid,
                'delivery_uuid' => $deliveryModel->uuid,
            ];

            if (isset($orderLine['id'])) {
                $controlData['id'] = $orderLine['id'];
            } else {
                $controlData['line_no'] = $orderLine['Line_No'];
            }

            $orderLineModel = OrderLine::updateOrCreate($controlData, [
                'description' => $orderLine['Description'],
                'qty_ordered' => $orderLine['Quantity'],
                'qty_received' => $orderLine['Quantity_Received'],
                'qty_to_fulfill' => $orderLine['qty_to_fulfill'],
            ]);

            $lineIds[] = $orderLineModel->id;
        }

        OrderLine::where(['delivery_uuid' => $deliveryModel->uuid, 'order_uuid' => $deliveryModel->order_uuid])->whereNotIn('id', $lineIds)->delete();

        return $deliveryModel;
    }
}
