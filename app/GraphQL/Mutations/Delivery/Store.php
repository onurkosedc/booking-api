<?php
namespace App\GraphQL\Mutations\Delivery;

use App\Mail\ReceivedDelivery;
use App\Models\Delivery;
use App\Models\DeliveryUnit;
use App\Models\Order;
use App\Models\OrderLine;

use Illuminate\Support\Facades\Auth;
use Closure;
use GraphQL;
use Illuminate\Support\Facades\Mail;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

use Illuminate\Http\Request;

class Store extends Mutation
{
    protected $attributes = ['name' => 'StoreDelivery'];

    protected $user;

    public function __construct(Request $request)
    {
        $this->user = $request->user();
    }

    public function authorize($root, array $args, $ctx, ResolveInfo $resolveInfo = null, Closure $getSelectFields = null): bool
    {
        return true; // Auth::check() ? $this->user->is_admin : false;
    }

    public function type(): Type
    {
        return GraphQL::type('delivery');
    }

    public function args(): array
    {
        return [
            'delivery' => ['name' => 'delivery', 'type' => Type::nonNull(GraphQL::type('delivery_input'))],
        ];
    }
    public function resolve($root, $args)
    {
        $delivery = $args['delivery'];

        $sendReceivedNotification = false;

        $deliveryModel = null;

        if (isset($delivery['uuid'])) {
            if (!empty($delivery['uuid'])) {
                $deliveryModel = Delivery::find($delivery['uuid']);
            }
        }

        $orderModel = Order::find($delivery['order_uuid']);

        switch (request()->getHost()) {
            case 'deliveries.yorkshiretrading.com':
                $domainIdentifier = 'ytc';
                break;
            case 'deliveries.totalfulfilment.co.uk':
                $domainIdentifier = 'tf';
                break;
            default:
                $domainIdentifier = 'ytc';
        }

        if (is_null($deliveryModel)) {
            $time = strtotime($delivery['from_time']);
            $endTime = date("H:i", strtotime("+{$delivery['processing_time']} minutes", $time));

            $deliveryModel = Delivery::create([
                'order_uuid' => $delivery['order_uuid'],
                'code_id' => $orderModel->code_id,
                'domain' => $domainIdentifier,
                'vendor_name' => $orderModel->vendor_name,
                'from_time' => $delivery['from_time'],
                'to_time' => $endTime,
                'delivery_date' => $delivery['date'],
                'processing_time' => $delivery['processing_time'],
                'confirmation_status' => 'pending'
            ]);

            $sendReceivedNotification = true;
        } else {
            $updates = [
                'domain' => $domainIdentifier,
                'from_time' => $delivery['from_time'],
                'to_time' => $delivery['to_time'],
                'delivery_date' => $delivery['date'],
                'processing_time' => $delivery['processing_time']
            ];

            $deliveryModel->fill($updates);

            $changes = $deliveryModel->getDirty();

            if (count($changes)) {
                if ($deliveryModel->confirmation_status != 'pending') {
                    $updates['confirmation_status'] = 'pending';
                    $updates['confirmation_changed_at'] = now();
                }

                if ($deliveryModel->is_canceled) {
                    $updates['is_canceled'] = false;
                    $updates['canceled_at'] = null;
                    $updates['is_cancellation_notified'] = false;
                    $updates['canceled_by'] = null;
                }

                $deliveryModel->fill($updates);
            }

            $deliveryModel->save();
        }

        $unitIds = [];

        foreach ($delivery['units'] as $unit) {
            DeliveryUnit::updateOrCreate([
                'delivery_uuid' => $deliveryModel->uuid,
                'unit_id' => $unit['id']
            ], [
                'quantity' => $unit['quantity'],
                'processing_time' => $unit['processing_time'],
            ]);

            $unitIds[] = $unit['id'];
        }

        $lineIds = [];

        foreach ($delivery['order_lines'] as $orderLine) {
            $controlData = [
                'order_uuid' => $deliveryModel->order_uuid,
                'delivery_uuid' => $deliveryModel->uuid,
            ];

            if (isset($orderLine['id'])) {
                $controlData['id'] = $orderLine['id'];
            } else {
                $controlData['line_no'] = $orderLine['Line_No'];
            }

            $orderLineModel = OrderLine::updateOrCreate($controlData, [
                'no' => $orderLine['No'],
                'description' => $orderLine['Description'],
                'qty_ordered' => $orderLine['Quantity'],
                'qty_received' => $orderLine['Quantity_Received'],
                'qty_to_fulfill' => $orderLine['qty_to_fulfill'],
            ]);

            $lineIds[] = $orderLineModel->id;
        }

        DeliveryUnit::where('delivery_uuid', $deliveryModel->uuid)->whereNotIn('unit_id', $unitIds)->delete();
        OrderLine::where(['delivery_uuid' => $deliveryModel->uuid, 'order_uuid' => $deliveryModel->order_uuid])->whereNotIn('id', $lineIds)->delete();

        if ($sendReceivedNotification) {
            $mail = Mail::to($deliveryModel->order->email);

            if (!empty($deliveryModel->order->email_alternate)) {
                $mail->cc($deliveryModel->order->email_alternate);
            }

            $mail->send(new ReceivedDelivery($deliveryModel));
        }

        return $deliveryModel;
    }
}
