<?php
namespace App\GraphQL\Mutations\Invitation;

use App\GraphQL\Concerns\NavFeedTrait;
use App\Mail\CancelDelivery;
use App\Models\FastTrack;
use GraphQL;
use Closure;
use PDF;
use App;

use Illuminate\Support\Facades\Auth;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Ramsey\Uuid\Uuid as RamseyUuid;
use Rebing\GraphQL\Support\Mutation;

use Illuminate\Http\Request;

class Send extends Mutation
{
    use NavFeedTrait;

    protected $attributes = ['name' => 'SendInvitation'];

    protected $user;

    public function __construct(Request $request)
    {
        $this->user = $request->user();
    }

    public function type(): Type
    {
        return GraphQL::type('generic_CRUD');
    }

    public function args(): array
    {
        return [
            'documentNumber' => ['name' => 'documentNumber', 'type' => Type::nonNull(Type::string())],
            'produceCode' => ['name' => 'produceCode', 'type' => Type::boolean()],
        ];
    }

    public function resolve($root, $args)
    {
        $orderHeadersQuery = $this->getBuilder()->from('Purchase_Order_Headers')->filterWhere('No')->equals($args['documentNumber'])->buildQuery();

        $orderHeaders = $this->http_response($orderHeadersQuery);

        if (isset($orderHeaders['timeout'])) {
            throw new GraphQL\Error\Error('feed_timeout');
        } else if (!isset($orderHeaders['value'])) {
            throw new GraphQL\Error\Error('missing_value');
        }

        $orderLinesQuery = $this->getBuilder()->from('Purchase_Order_Lines')->filterWhere('Document_No')->equals($args['documentNumber'])->buildQuery();

        $orderLines = $this->http_response($orderLinesQuery);

        if (isset($orderLines['timeout'])) {
            throw new GraphQL\Error\Error('feed_timeout');
        } else if (!isset($orderLines['value'])) {
            throw new GraphQL\Error\Error('missing_value');
        }

        $code = $args['produceCode'] ? FastTrack::create([
            'code' => substr(RamseyUuid::uuid4()->toString(), 9, 14),
            'email' => null
        ]) : null;

        $fileName = preg_replace("/\s+/", '', $orderHeaders['value'][0]['Buy_from_Vendor_No'] . '-' . $orderHeaders['value'][0]['No']);

        $pdf = app('dompdf.wrapper');
        $pdf->getDomPDF()->set_option('enable_php', true);

        $data = [
            'order' => $orderHeaders['value'][0],
            'lines' => $orderLines['value'],
            'produceCode' => $args['produceCode'],
            'code' => $code
        ];

        $pdf->loadView('pdfs.invitation', $data);

        $pdf->output();

        $pdf->save('files/' . $fileName . '.pdf');

        //->setPaper('a4', 'portrait')

        /*$mail = Mail::to($args['email']);

        if (!empty($args['emailAlternate'])) {
            $mail->cc($args['emailAlternate']);
        }

        $mail->send(new CancelDelivery($deliveryModel, $delivery['reason']));*/

        return [
            'is_sent' => true,
            'file_url' => $fileName . '.pdf'
        ];
    }
}
