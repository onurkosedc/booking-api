<?php
namespace App\GraphQL\Mutations\Setting;

use App\GraphQL\Concerns\GetOrigin;
use App\Models\Company;

use Closure;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Illuminate\Support\Facades\Auth;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;

use Illuminate\Http\Request;

class Store extends Mutation
{
    use GetOrigin;

    protected $attributes = ['name' => 'StoreSettings'];

    protected $user;

    public function __construct(Request $request)
    {
        $this->user = $request->user();

        $this->getOriginHost($request);
    }

    public function authorize($root, array $args, $ctx, ResolveInfo $resolveInfo = null, Closure $getSelectFields = null): bool
    {
        return Auth::check() ? $this->user->is_admin : false;
    }

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('setting'));
    }

    public function args(): array
    {
        return [
            'settings' => ['name' => 'settings', 'type' => Type::nonNull(Type::listOf(GraphQL::type('setting_input')))],
        ];
    }
    public function resolve($root, $args)
    {
        if (is_array($args['settings'])) {
            $company = Company::firstOrCreate([
                'name' => $this->domain
            ]);

            foreach ($args['settings'] as $setting) {
                $company->settings()->set($setting['field'], $setting['value']);
            }
        }
    }
}
