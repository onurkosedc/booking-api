<?php
namespace App\GraphQL\Mutations\Setting;

use App\GraphQL\Concerns\GetOrigin;
use App\Models\Company;
use Illuminate\Support\Facades\Auth;
use Closure;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;

use Illuminate\Http\Request;
use Rebing\GraphQL\Support\UploadType;

class UploadFile extends Mutation
{
    use GetOrigin;

    protected $attributes = ['name' => 'UploadFile'];

    protected $user;

    public function __construct(Request $request)
    {
        $this->user = $request->user();

        $this->getOriginHost($request);
    }

    public function authorize($root, array $args, $ctx, ResolveInfo $resolveInfo = null, Closure $getSelectFields = null): bool
    {
        return Auth::check() ? $this->user->is_admin : false;
    }

    public function type(): Type
    {
        return GraphQL::type('file');
    }

    public function args(): array
    {
        return [
            'file' => ['name' => 'file', 'type' => Type::nonNull(new UploadType('FileUpload'))],
            'for' => ['name' => 'for', 'type' => Type::nonNull(Type::string())],
        ];
    }

    public function resolve($root, $args)
    {
        $file = data_get($args, 'file.0');
        $for = data_get($args, 'for');

        $company = Company::firstOrCreate([
            'name' => $this->domain
        ]);

        $oldFilename = $company->settings()->get($for);

        if (!empty($oldFilename)) {
            Storage::delete('public/' . $oldFilename);

            $oldFilenameParts = explode('.', $oldFilename);

            Storage::delete('public/' . implode('_thumbnail.', $oldFilenameParts));
        }

        $newFilename = Str::random(10);
        $extension = strtolower($file->getClientOriginalExtension());

        Storage::putFileAs('public', $file, $newFilename . '.' . $extension);

        $image = Image::make($file);

        $image->resize(150, 150, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        $image->save();

        $canvas = Image::canvas(150, 150);

        $canvas->fill(storage_path('app/public/checker.png'));

        $canvas->insert($image, 'center');

        $canvas->save(storage_path('app/public/' . $newFilename . '_thumbnail.' . $extension));

        $company->settings()->set($for, $newFilename . '.' . $extension);

        return [
            'name' => $newFilename . '.' . $extension,
            'for' => $for
        ];
    }
}
