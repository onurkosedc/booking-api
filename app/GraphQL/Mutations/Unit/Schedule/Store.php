<?php
namespace App\GraphQL\Mutations\Unit\Schedule;

use App\Models\Schedule;
use App\Models\Unit;

use Auth;
use Closure;
use GraphQL;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

use Illuminate\Http\Request;

class Store extends Mutation
{
    protected $attributes = ['name' => 'StoreSchedule'];

    protected $user;

    public function __construct(Request $request)
    {
        $this->user = $request->user();
    }

    public function authorize($root, array $args, $ctx, ResolveInfo $resolveInfo = null, Closure $getSelectFields = null): bool
    {
        return Auth::check() ? $this->user->is_admin : false;
    }

    public function type(): Type
    {
        return GraphQL::type('unit_schedule');
    }

    public function args(): array
    {
        return [
            'schedule' => ['name' => 'schedule', 'type' => Type::nonNull(GraphQL::type('unit_schedule_input'))],
        ];
    }
    public function resolve($root, $args)
    {
        $schedule = $args['schedule'];

        //$checkTimeConflict = Schedule::where('unit_id', $schedule['unitId'])->where()

        $scheduleModel = Schedule::updateOrCreate([
            'id' => $schedule['id'],
        ], [
            'unit_id' => $schedule['unitId'],
            'period' => $schedule['period'],
            'from_time' => $schedule['fromTime'],
            'to_time' => $schedule['toTime'],
            'from_date' => empty($schedule['fromDate']) ? null : $schedule['fromDate'],
            'to_date' => empty($schedule['toDate']) ? null : $schedule['toDate'],
        ]);

        /* if ($scheduleModel->only) {
            Schedule::where([
                'unit_id' => $scheduleModel->unit_id,
                'only' => true,
                'from_date' => $scheduleModel->from_date
            ])
                ->where('id', '!=', $scheduleModel->id)
                ->update([
                    'only' => false
                ]);
        } */

        return $scheduleModel;
    }
}
