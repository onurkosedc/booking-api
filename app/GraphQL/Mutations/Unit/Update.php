<?php
namespace App\GraphQL\Mutations\Unit;

use App\Models\Unit;

use Auth;
use Closure;
use GraphQL;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

use Illuminate\Http\Request;

class Update extends Mutation
{
    protected $attributes = ['name' => 'UpdateUnit'];

    protected $user;

    public function __construct(Request $request)
    {
        $this->user = $request->user();
    }

    public function authorize($root, array $args, $ctx, ResolveInfo $resolveInfo = null, Closure $getSelectFields = null): bool
    {
        return Auth::check() ? $this->user->is_admin : false;
    }

    public function type(): Type
    {
        return GraphQL::type('unit');
    }

    public function args(): array
    {
        return [
            'unit' => ['name' => 'unit', 'type' => Type::nonNull(GraphQL::type('unit_input'))],
        ];
    }
    public function resolve($root, $args)
    {
        $unit = $args['unit'];

        $unitModel = Unit::find($unit['id']);

        if (is_null($unitModel)) {
            throw with(new GraphQL\Error\Error('not_found'));
        }

        $unitModel->update([
            'code' => $unit['code'],
            'name' => $unit['name'],
        ]);

        return $unitModel;
    }
}
