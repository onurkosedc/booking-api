<?php
namespace App\GraphQL\Mutations\BusinessHour;

use App\Models\BusinessHour;
use App\Models\Unit;

use Auth;
use Closure;
use GraphQL;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

use Illuminate\Http\Request;

class Populate extends Mutation
{
    protected $attributes = ['name' => 'PopulateBusinessHours'];

    protected $user;

    public function __construct(Request $request)
    {
        $this->user = $request->user();
    }

    public function authorize($root, array $args, $ctx, ResolveInfo $resolveInfo = null, Closure $getSelectFields = null): bool
    {
        return Auth::check() ? $this->user->is_admin : false;
    }

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('business_hour'));
    }

    public function args(): array
    {
        return [
            'isWeekendsOff' => ['name' => 'isWeekendsOff', 'type' => Type::boolean()],
            'hours' => ['name' => 'hours', 'type' => Type::nonNull(GraphQL::type('business_hour_input'))],
        ];
    }
    public function resolve($root, $args)
    {
        $hours = $args['hours'];

        for ($i = 1; $i <= 7; $i++) {
            BusinessHour::updateOrCreate([
                'day' => $i
            ], [
                'on' => $args['isWeekendsOff'] == true && ($i == 6 || $i == 7) ? false : true,
                'start_at' => $hours['start_at'],
                'end_at' => $hours['end_at'],
                'created_by' => $this->user->id
            ]);
        }

        $hours = BusinessHour::get();

        return $hours;
    }
}
