<?php
namespace App\GraphQL\Mutations\BusinessHour;

use App\Models\BusinessHour;

use Auth;
use GraphQL;
use Closure;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

use Illuminate\Http\Request;

class Delete extends Mutation
{
    protected $attributes = ['name' => 'DeleteBusinessHour'];

    protected $user;

    public function __construct(Request $request)
    {
        $this->user = $request->user();
    }

    public function authorize($root, array $args, $ctx, ResolveInfo $resolveInfo = null, Closure $getSelectFields = null): bool
    {
        return Auth::check() ? $this->user->is_admin : false;
    }

    public function type(): Type
    {
        return GraphQL::type('generic_CRUD');
    }

    public function args(): array
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::nonNull(Type::int())],
        ];
    }
    public function resolve($root, $args)
    {
        $hourModel = BusinessHour::find($args['id']);

        if (is_null($hourModel)) {
            throw with(new GraphQL\Error\Error('not_found'));
        }

        $hourModel->delete();

        return ['is_deleted' => true];
    }
}
