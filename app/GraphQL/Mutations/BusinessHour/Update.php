<?php
namespace App\GraphQL\Mutations\BusinessHour;

use App\Models\BusinessHour;
use App\Models\Unit;

use Auth;
use GraphQL;
use Closure;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

use Illuminate\Http\Request;

class Update extends Mutation
{
    protected $attributes = ['name' => 'UpdateBusinessHours'];

    protected $user;

    public function __construct(Request $request)
    {
        $this->user = $request->user();
    }

    public function authorize($root, array $args, $ctx, ResolveInfo $resolveInfo = null, Closure $getSelectFields = null): bool
    {
        return Auth::check() ? $this->user->is_admin : false;
    }

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('business_hour'));
    }

    public function args(): array
    {
        return [
            'hours' => ['name' => 'hours', 'type' => Type::nonNull(Type::listOf(GraphQL::type('business_hour_input')))],
        ];
    }
    public function resolve($root, $args)
    {
        foreach ($args['hours'] as $hour) {
            if (empty($hour['date']) && empty($hour['day'])) {
                throw with(new GraphQL\Error\Error('missing_date_or_day'));
            }
        }

        foreach ($args['hours'] as $hour) {
            if ($hour['id'] == -1) {
                $hour['id'] = null;

                $hourModel = BusinessHour::create($hour);
            } else {
                $hourModel = BusinessHour::find($hour['id']);

                $hourModel->update($hour);
            }
        }

        return BusinessHour::get();
    }
}
