<?php
namespace App\GraphQL\Mutations\FastTrack;

use App\Models\Delivery;
use App\Models\FastTrack;

use App\Models\Order;
use Auth;
use GraphQL;
use Closure;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

use Illuminate\Http\Request;

class Delete extends Mutation
{
    protected $attributes = ['name' => 'DeleteFastTrack'];

    protected $user;

    public function __construct(Request $request)
    {
        $this->user = $request->user();
    }

    public function authorize($root, array $args, $ctx, ResolveInfo $resolveInfo = null, Closure $getSelectFields = null): bool
    {
        return Auth::check() ? $this->user->is_admin : false;
    }

    public function type(): Type
    {
        return GraphQL::type('generic_CRUD');
    }

    public function args(): array
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::int()],
            'clear' => ['name' => 'clear', 'type' => Type::boolean()],
        ];
    }
    public function resolve($root, $args)
    {
        if (isset($args['id'])) {
            $codeModel = FastTrack::find($args['id']);

            if (is_null($codeModel)) {
                throw with(new GraphQL\Error\Error('not_found'));
            }

            /* $checkDeliveries = Delivery::where('code_id', $args['id'])->exists();
            $checkOrders = Order::where('code_id', $args['id'])->exists();

            if ($checkDeliveries ||$checkOrders) {
                throw with(new GraphQL\Error\Error('already_used'));
            } */

            $codeModel->delete();
        } elseif (isset($args['clear'])) {
            FastTrack::truncate();
        }

        return ['is_deleted' => true];
    }
}
