<?php
namespace App\GraphQL\Mutations\FastTrack;

use App\Mail\FastTrackCreation;
use App\Models\FastTrack;

use Mail;
use Auth;
use Closure;
use GraphQL;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Ramsey\Uuid\Uuid as RamseyUuid;
use Rebing\GraphQL\Support\Mutation;

use Illuminate\Http\Request;

class Store extends Mutation
{
    protected $attributes = ['name' => 'StoreFastTrack'];

    protected $user;

    public function __construct(Request $request)
    {
        $this->user = $request->user();
    }

    public function authorize($root, array $args, $ctx, ResolveInfo $resolveInfo = null, Closure $getSelectFields = null): bool
    {
        return Auth::check() ? $this->user->is_admin : false;
    }

    public function type(): Type
    {
        return GraphQL::type('fast_track');
    }

    public function args(): array
    {
        return [
            'email' => ['name' => 'email', 'type' => Type::string()],
        ];
    }
    public function resolve($root, $args)
    {
        $code = FastTrack::create([
            'code' => substr(RamseyUuid::uuid4()->toString(), 9, 14),
            'email' => $args['email'],
            'created_by' => $this->user->id
        ]);

        if (isset($args['email'])) {
            if (!empty($args['email'])) {
                $mail = Mail::to($args['email']);

                $mail->send(new FastTrackCreation($code));
            }
        }

        return $code;
    }
}
