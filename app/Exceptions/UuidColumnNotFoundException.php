<?php

namespace App\Exceptions;

use Exception;

class UuidColumnNotFoundException extends Exception
{
}
